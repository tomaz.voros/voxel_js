attribute vec3 a_position;
attribute vec2 a_texture_position;

uniform mat4 u_MVP;

varying vec2 v_texture_position;

void main() {
  gl_Position = u_MVP * vec4(a_position, 1.0);
  v_texture_position = a_texture_position * 0.0078125; // / 128.0
}
