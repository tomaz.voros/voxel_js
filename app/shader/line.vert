attribute vec3 a_position;

uniform mat4 u_MVP;
uniform vec3 u_offset;

void main() {
  gl_Position = u_MVP * vec4(a_position + u_offset, 1.0);
}
