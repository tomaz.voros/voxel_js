#version 300 es

precision mediump float;

uniform sampler2D u_texture;

in vec2 v_texture_position;
in vec4 v_ambient_occlusion;
in vec2 v_ao_position;

out vec4 out_frag_color;

void main() {
  vec4 texture_color = texture(u_texture, v_texture_position);
  if (texture_color.a < 0.5)
    discard;

  // bilinear interpolation
  float x = mix(v_ambient_occlusion.x, v_ambient_occlusion.y, v_ao_position.x);
  float y = mix(v_ambient_occlusion.z, v_ambient_occlusion.w, v_ao_position.x);
  float shade = mix(x, y, v_ao_position.y);

  out_frag_color = vec4(texture_color.rgb * shade, 1.0);
}
