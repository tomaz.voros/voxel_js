precision mediump float;

uniform sampler2D u_texture;

varying vec2 v_texture_position;

void main() {
  vec4 texture_color = texture2D(u_texture, v_texture_position);
  if (texture_color.a < 0.5)
    discard;
  gl_FragColor = texture_color;
}
