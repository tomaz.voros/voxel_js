#version 300 es

in vec3 a_position;
in uint a_ambient_occlusion;
in vec2 a_texture_position;

uniform mat4 u_MVP;
uniform vec3 u_offset;

out vec2 v_texture_position;
out vec4 v_ambient_occlusion;
out vec2 v_ao_position;

const float AO_SHADE_STRENGTH = 0.25;

void main() {
  gl_Position = u_MVP * vec4(a_position + u_offset, 1.0);
  v_texture_position = a_texture_position * 0.0078125; // / 128.0
  v_ambient_occlusion = vec4(
    a_ambient_occlusion >> 6u & 3u,
    a_ambient_occlusion >> 4u & 3u,
    a_ambient_occlusion >> 2u & 3u,
    a_ambient_occlusion >> 0u & 3u
  );
  v_ambient_occlusion = 1.0 - AO_SHADE_STRENGTH * v_ambient_occlusion;
  int hi = (gl_VertexID >> 1) & 1;
  v_ao_position = vec2((gl_VertexID & 1) != hi, hi);
}
