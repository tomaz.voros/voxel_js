// ISSUE(0)
// import Algebra from './Algebra.js'

export default class Camera {
  constructor() {
    this._position = Algebra.v3()
    this._position[0] = 0
    this._position[1] = 0
    this._position[2] = 0
    this._facing = Algebra.v3()
    this._up = Algebra.v3()
    this._up[1] = 1
    this._rotation = Algebra.v3()
    this._rotation[0] = 1

    this._view = Algebra.m4()
    this._projection = Algebra.m4()
    this._view_projection = Algebra.m4()

    this._fov = 1.5
    this._near_z = 0.1
    this._far_z = 250
    this._aspect_ratio = 1
    this._yaw = 0
    this._pitch = 0

    this._max_pitch = Math.PI / 2 - 0.01

    this.ortho = false
  }

  get yaw() {
    return this._yaw
  }

  get pitch() {
    return this._pitch
  }

  get view_projection() {
    return this._view_projection
  }

  get aspect() {
    return this._aspect_ratio
  }

  get world_up() {
    return this._up
  }

  get position() {
    return this._position
  }

  get facing() {
    return this._facing
  }

  get rotation() {
    return this._rotation
  }

  ray_from_screen(ray_direction, position_on_screen) {
    // TODO: simplify this (if possible)
    const f = this._near_z
    const hs = 2 * f * Math.tan(this._fov / 2)
    const ws = this._aspect_ratio * hs
    const w = [-this._facing[0], -this._facing[1], -this._facing[2]]
    Algebra.normal_v3(w, w)
    const u = [0, 0, 0]
    Algebra.cross_v3(u, this._up, w)
    Algebra.normal_v3(u, u)
    const v = [0, 0, 0]
    Algebra.cross_v3(v, w, u)
    Algebra.normal_v3(v, v)
    ray_direction[0] = -w[0] * f
    ray_direction[1] = -w[1] * f
    ray_direction[2] = -w[2] * f
    Algebra.mul_v3_s(u, u, ws / 2)
    Algebra.mul_v3_s(v, v, hs / 2)
    Algebra.sub_v3(ray_direction, ray_direction, v)
    Algebra.sub_v3(ray_direction, ray_direction, u)
    Algebra.mul_v3_s(u, u, 2 * position_on_screen[0])
    Algebra.mul_v3_s(v, v, 2 * position_on_screen[1])
    Algebra.add_v3(ray_direction, ray_direction, u)
    Algebra.add_v3(ray_direction, ray_direction, v)
    Algebra.normal_v3(ray_direction, ray_direction)
    return ray_direction
  }

  update_aspect(aspect) {
    // TODO: the camera should be scaled, so that you can always see at least (-1,-1)-(1,1) (same issue with GUI)
    this._aspect_ratio = aspect
    let fov = this._fov
    if (this.ortho)
      Algebra.ortho_m4(this._projection, fov * 15, aspect, this._near_z, this._far_z)
    else
      Algebra.perspective_m4(this._projection, fov, aspect, this._near_z, this._far_z)
    this._update_view_projection()
  }

  set_view(position, yaw, pitch) {
    this._position[0] = 0
    this._position[1] = 0
    this._position[2] = 0
    this._yaw = 0
    this._pitch = 0
    this.update_view(position, yaw, pitch)
  }

  /**
   * @returns Object with mat4 (view_projection sans int_translation) and vec3 (int_translation)
   */
  compute_and_get_view_projection_and_int_translation() {
    const result = {
      view_projection: Algebra.m4(),
      int_translation: Algebra.v3()
    }

    result.int_translation[0] = Math.floor(this._position[0])
    result.int_translation[1] = Math.floor(this._position[1])
    result.int_translation[2] = Math.floor(this._position[2])
    const frac_translation = Algebra.sub_v3(Algebra.v3(), this._position, result.int_translation)
    const at = Algebra.add_v3(Algebra.v3(), this._facing, frac_translation)
    Algebra.view_m4(result.view_projection, frac_translation, at, this._up)
    Algebra.mul_m4(result.view_projection, this._projection, result.view_projection)

    return result
  }

  update_view(delta_movement, delta_yaw, delta_pitch) {
    this._yaw += delta_yaw
    this._pitch += delta_pitch
    this._pitch = Algebra.clamp_s(this._pitch, -this._max_pitch, this._max_pitch)
    this._yaw %= Math.PI * 2

    this._position[0] += delta_movement[0]
    this._position[1] += delta_movement[1]
    this._position[2] += delta_movement[2]

    this._rotation[0] = Math.cos(this._yaw)
    this._rotation[1] = 0
    this._rotation[2] = Math.sin(this._yaw)

    this._facing[0] = Math.cos(this._pitch) * this._rotation[0]
    this._facing[1] = Math.sin(this._pitch)
    this._facing[2] = Math.cos(this._pitch) * this._rotation[2]

    const at = [
      this._facing[0] + this._position[0],
      this._facing[1] + this._position[1],
      this._facing[2] + this._position[2]
    ]

    Algebra.view_m4(this._view, this._position, at, this._up)

    this._update_view_projection()
  }

  _update_view_projection() {
    Algebra.mul_m4(this._view_projection, this._projection, this._view)
  }
}
