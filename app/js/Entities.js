import ShaderProgram from "./ShaderProgram.js";

class EntityBase {
    constructor(id) {
      /** @type {number} */
      this.id = id
      /** @type {Array<number>} */
      this.position = [0, 0, 0]
      /** @type {Array<number>} */
      this.color = [0.5, 0.5, 0.5]
    }

    /**
     * @param {WebGLRenderingContext} gl
     * @param {WebGLUniformLocation} offset_uniform
     * @param {number} tick
     * @param {number} t_since_tick
     * @param {WebGLUniformLocation} color_uniform
     */
    draw(gl, offset_uniform, tick, t_since_tick, color_uniform) {
      gl.uniform3f(color_uniform, this.color[0], this.color[1], this.color[2])
      gl.uniform3f(offset_uniform, this.position[0], this.position[1], this.position[2])
      gl.drawArrays(gl.TRIANGLES, 0, 3 * 12)
    }

    /**
     * 
     * @param {DataView} data
     * @param {number} offset Offset into data where the update data is extracted.
     * @returns {number} Offset to one past the end of the read update data aka. offset for the next entity.
     */
    deserialize_update(data, offset, tick) {
      return offset
    }
}

class EntityPlayer extends EntityBase {
  constructor(id) {
    super(id)
    /** @type {number} */
    this.yaw = 0
    /** @type {number} */
    this.pitch = 0
    /** @type {number} */
    this.yaw_old = 0
    /** @type {number} */
    this.pitch_old = 0
    /** @type {Array<number>} */
    this.position_old = [0, 0, 0]
    /** @type {number} */
    this.tick = 0
    /** @type {number} */
    this.tick_old = -1

    this.color[0] = 0.6
    this.color[1] = 0.7
    this.color[2] = 0.5
  }

  draw(gl, offset_uniform, tick, t_since_tick, color_uniform) {
    const pos = Algebra.v3()
    Algebra.lerp_snapshots_v3(
      pos,
      this.position_old, this.position,
      this.tick_old, this.tick,
      tick, t_since_tick, cfg.TICK_DT
    )
    gl.uniform3f(color_uniform, this.color[0], this.color[1], this.color[2])
    gl.uniform3f(offset_uniform, pos[0], pos[1], pos[2])
    gl.drawArrays(gl.TRIANGLES, 0, 3 * 12)
  }

  deserialize_update(data, offset, tick) {
    this.position_old[0] = this.position[0]
    this.position_old[1] = this.position[1]
    this.position_old[2] = this.position[2]
    this.yaw_old = this.yaw
    this.pitch_old = this.pitch
    this.tick_old = this.tick

    this.position[0] = data.getFloat32(offset, true)
    this.position[1] = data.getFloat32(offset + 4, true)
    this.position[2] = data.getFloat32(offset + 8, true)
    this.yaw = data.getFloat32(offset + 12, true)
    this.pitch = data.getFloat32(offset + 16, true)
    this.tick = tick

    return offset + 20
  }
}

class EntityBlob extends EntityBase {
  constructor(id) {
    super(id)
    /** @type {Array<number>} */
    this.position_old = [0, 0, 0]
    /** @type {number} */
    this.tick = 0
    /** @type {number} */
    this.tick_old = -1

    this.color[0] = 0.7
    this.color[1] = 0.3
    this.color[2] = 0.6
  }

  draw(gl, offset_uniform, tick, t_since_tick, color_uniform) {
    const pos = Algebra.v3()
    Algebra.lerp_snapshots_v3(
      pos,
      this.position_old, this.position,
      this.tick_old, this.tick,
      tick, t_since_tick, cfg.TICK_DT
    )
    gl.uniform3f(color_uniform, this.color[0], this.color[1], this.color[2])
    gl.uniform3f(offset_uniform, pos[0], pos[1], pos[2])
    gl.drawArrays(gl.TRIANGLES, 0, 3 * 12)
  }

  deserialize_update(data, offset, tick) {
    this.position_old[0] = this.position[0]
    this.position_old[1] = this.position[1]
    this.position_old[2] = this.position[2]
    this.tick_old = this.tick
    this.position[0] = data.getFloat32(offset, true)
    this.position[1] = data.getFloat32(offset + 4, true)
    this.position[2] = data.getFloat32(offset + 8, true)
    this.tick = tick
    return offset + 12
  }
}

class EntityFactory {
  /**
   * Creates a new entity based on type parameter.
   * @param {number} type Type number.
   * @returns {EntityBase}
   */
  static create(type) {
    switch (type) {
      case cfg.ENTITY_TYPE.PLAYER:
        return new EntityPlayer()
      case cfg.ENTITY_TYPE.BLOB:
        return new EntityBlob()
      default:
        console.log("Invalid entity type, creating base.")
        // fallthrough
      case cfg.ENTITY_TYPE.BASE:
        return new EntityBase()
    }
  }
}

// maybe rename to entity container or manager or similar
export default class Entities {
  /**
   * @param {WebGLRenderingContext} gl
   * @param {*} assets
   */
  constructor(gl, assets) {
    // TODO: merge this.entities and this.entity_ids Map() is probably not needed, just Array() with [id, entity] as elements
    /** @type {Map<number,EntityBase>} */
    this.entities = new Map()
    /** @type {Array<number>} */
    this.entity_ids = new Array()
    /** @type {number} */
    this.self_id = 0

    this.assets = assets

    this.entity_shader = new ShaderProgram(
      gl,
      [
          { type: gl.VERTEX_SHADER, source: assets.entity_vert },
          { type: gl.FRAGMENT_SHADER, source: assets.entity_frag }
      ]
  )

    const vertices = [
        -1.0,-1.0,-1.0,  -1.0,-1.0, 1.0,  -1.0, 1.0, 1.0,
         1.0, 1.0,-1.0,  -1.0,-1.0,-1.0,  -1.0, 1.0,-1.0,
         1.0,-1.0, 1.0,  -1.0,-1.0,-1.0,   1.0,-1.0,-1.0,
         1.0, 1.0,-1.0,   1.0,-1.0,-1.0,  -1.0,-1.0,-1.0,
        -1.0,-1.0,-1.0,  -1.0, 1.0, 1.0,  -1.0, 1.0,-1.0,
         1.0,-1.0, 1.0,  -1.0,-1.0, 1.0,  -1.0,-1.0,-1.0,
        -1.0, 1.0, 1.0,  -1.0,-1.0, 1.0,   1.0,-1.0, 1.0,
         1.0, 1.0, 1.0,   1.0,-1.0,-1.0,   1.0, 1.0,-1.0,
         1.0,-1.0,-1.0,   1.0, 1.0, 1.0,   1.0,-1.0, 1.0,
         1.0, 1.0, 1.0,   1.0, 1.0,-1.0,  -1.0, 1.0,-1.0,
         1.0, 1.0, 1.0,  -1.0, 1.0,-1.0,  -1.0, 1.0, 1.0,
         1.0, 1.0, 1.0,  -1.0, 1.0, 1.0,   1.0,-1.0, 1.0,
    ]

    this.vertex_count = 3 * 12

    this.vertex_buffer = gl.createBuffer()
    gl.bindBuffer(gl.ARRAY_BUFFER, this.vertex_buffer)
    gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(vertices), gl.STATIC_DRAW)
  }

  set_self_id(self_id) {
    this.self_id = self_id
  }

  update(message, tick) {
    const data_view = new DataView(message)
    if (data_view.byteLength < 1) {
      console.log("Empty update entities message.")
      return
    }
    const message_id = data_view.getUint8(0)
    if (message_id !== cfg.MESSAGE_ID.ENTITY_DATA) {
      console.log('Wrong message id update entities.')
      return
    }
    let data_view_offset = 1
    for (const entity_id of this.entity_ids) {
      if (!this.entities.has(entity_id)) {
        console.log("Internal Entities error.");
        continue
      }
      const entity = this.entities.get(entity_id)

      data_view_offset = entity.deserialize_update(data_view, data_view_offset, tick)
      if (data_view_offset > data_view.byteLength)
        break
    }
    if (data_view_offset != data_view.byteLength) {
      console.log("Bad Entity update message.")
      return
    }
  }

  add(message) {
    const data_view = new DataView(message)
    if (data_view.byteLength < 1) {
      console.log("Empty add entities message.")
      return
    }
    const message_type = data_view.getUint8(0)
    if (message_type != cfg.MESSAGE_ID.ADD_ENTITIES) {
      console.log("Wrong add entities message.")
      return
    }

    if ((data_view.byteLength - 1) % 8 !== 0) {
      console.log("Bad add entities message.")
      return
    }

    const len = (data_view.byteLength - 1) / 8

    // TODO: abstract algorithm, maybe into generator or something (similar issue on the server side)
    let array_i = 0;
    for (let i = 0; i < len; ++i) {
      const entity_id = data_view.getInt32(i * 8 + 1, true)
      const entity_type = data_view.getInt32(i * 8 + 5, true)
      while (array_i < this.entity_ids.length && this.entity_ids[array_i] < entity_id) {
        ++array_i;
      }
      if (this.entity_ids[array_i] == entity_id) {
        console.log("Bad entity add.")
        continue
      }
      if (this.entities.has(entity_id)) {
        console.log("Internal Entities error entity_id already exists.")
        continue
      }
      this.entity_ids.splice(array_i, 0, entity_id)
      this.entities.set(entity_id, EntityFactory.create(entity_type))
    }
  }

  remove(message) {
    const data_view = new DataView(message)
    if (data_view.byteLength < 1) {
      console.log("Empty remove entities message.")
      return
    }
    const message_type = data_view.getUint8(0)
    if (message_type != cfg.MESSAGE_ID.REMOVE_ENTITIES) {
      console.log("Wrong remove entities message.")
      return
    }

    if ((data_view.byteLength - 1) % 4 !== 0) {
      console.log("Bad remove entities message.")
      return
    }

    const len = (data_view.byteLength - 1) / 4

    // TODO: abstract algorithm, maybe into generator or something (similar issue on the server side)
    let array_i = 0;
    for (let i = 0; i < len; ++i) {
      const entity_id = data_view.getInt32(i * 4 + 1, true)
      while (array_i < this.entity_ids.length && this.entity_ids[array_i] != entity_id) {
        ++array_i;
      }
      if (this.entity_ids.length == array_i) {
        console.log("Bad entity delete.")
        break
      }
      if (!this.entities.has(entity_id)) {
        console.log("Internal Entities error entity_id does not exists.")
        continue
      }
      this.entity_ids.splice(array_i, 1)
      this.entities.delete(entity_id)
    }
  }

  /**
   * 
   * @param {WebGLRenderingContext} gl
   * @param {number} t_since_tick
   * @param {number[]} VP
   */
  draw(gl, t_since_tick, VP, tick) {
    const shader = this.entity_shader

    gl.useProgram(shader.program)

    gl.uniformMatrix4fv(shader.uniforms.u_MVP, false, VP)
    gl.bindBuffer(gl.ARRAY_BUFFER, this.vertex_buffer)

    gl.vertexAttribPointer(shader.attributes.a_position, 3, gl.FLOAT, false, 12, 0)
    gl.enableVertexAttribArray(shader.attributes.a_position)
    for (const [id, entity] of this.entities) {
      if (id == this.self_id)
        continue
      entity.draw(gl, shader.uniforms.u_offset, tick, t_since_tick, shader.uniforms.u_color)
    }

    gl.disableVertexAttribArray(shader.attributes.a_position)
  }
}
