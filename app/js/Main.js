import Assets from './Assets.js'
import GUI from './GUI.js'
import Voxel from './Voxel.js'
import Input from './Input.js'
import { Monostable, MonostableToggle } from './LogicGates.js'
import ShaderProgram from './ShaderProgram.js'
import QuadEBO from './QuadEBO.js'
import Entities from './Entities.js'
import Camera from './Camera.js'
import LineSquare from './LineSquare.js'
// ISSUE(0)
// import cfg from './cfg.js'

export default async function init_run(name, url) {
    const assets = await Assets.load()
    const atlas_data = Assets.generate_atlas_data(assets.assets)
    // TODO: cache atlas, offer user to select different texture packages
    const atlas = await Assets.generate_atlas(
        atlas_data.tiles,
        atlas_data.tiles_per_row,
        atlas_data.tile_width
    )
    assets['generated_atlas'] = atlas
    assets['generated_transp'] = atlas_data.transparent
    assets['generated_coords'] = atlas_data.coords

    // ISSUE(0)
    const web_worker = new Worker('/js/Logic.js', /* { type: 'module' } */)
    web_worker.postMessage({
        type: 5,
        user_name: name,
        server_url: url,
        tex_coord: assets.generated_coords,
        tex_transp: assets.generated_transp
    })
    web_worker.onmessage = message => {
        if (message.data.type !== 7) {
            console.log('Worker start failed.')
            return
        }
        window['main'] = new Main(assets, web_worker)
    }
}

/**
 * @typedef {Object} ViewPort
 * @property {number} x
 * @property {number} y
 * @property {number} width
 * @property {number} height
 */

class Main {
    /**
     * @param {Assets} assets Assets.
     * @param {Worker} web_worker Web worker.
     */
    constructor(assets, web_worker) {
        this.assets = assets
        this.initial_orientation_received = false

        this.mouse_sensitivity = 0.005
        this.camera_speed = 5
        this.selected_block = 0

        this.tick_value = 0
        this.tick_time = Timer.now()

        this.line_cube_position = [0, 0, 0]
        this.ray_queue_length = 0
        
        this.canvas = document.querySelector('canvas')
        const gl = this.canvas.getContext('webgl2', { antialias: false })
        /** @type {WebGL2RenderingContext} */
        this.gl = gl
        this.canvas.width = window.innerWidth
        this.canvas.height = window.innerHeight
        window.addEventListener('resize', () => {
            this.canvas.width = window.innerWidth
            this.canvas.height = window.innerHeight
        })
        this.status_span = document.querySelector('#status')
        this.info_span = document.querySelector('#info')
        this.line_shader = new ShaderProgram(
            gl,
            [ { type: gl.VERTEX_SHADER, source: assets.line_vert },
            { type: gl.FRAGMENT_SHADER, source: assets.line_frag } ]
        )

        this.line_square = new LineSquare(gl)
        this.quad_ebo = new QuadEBO(gl)
        this.voxel_texture_atlas = gl.createTexture()
        gl.bindTexture(gl.TEXTURE_2D, this.voxel_texture_atlas)
        gl.pixelStorei(gl.UNPACK_FLIP_Y_WEBGL, true)
        gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, gl.RGBA, gl.UNSIGNED_BYTE, assets.generated_atlas)
        // TODO: make TEXTURE_MAX_LEVEL depend on tiles_per_row in atlas
        gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAX_LEVEL, 4)
        gl.generateMipmap(gl.TEXTURE_2D)
        gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.NEAREST_MIPMAP_LINEAR)
        gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.NEAREST)
        gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_S, gl.CLAMP_TO_EDGE)
        gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_T, gl.CLAMP_TO_EDGE)
        // save memory, delete texture from RAM
        this.assets.generated_atlas = null

        this.worker = web_worker
        this.worker.onmessage = message => this._process_message_from_worker(message)

        this.camera = new Camera()
        this.voxel = new Voxel(this.gl, assets, this.quad_ebo, this.voxel_texture_atlas)
        this.entities = new Entities(this.gl, assets)
        this.gui = new GUI(this.gl, assets, this.quad_ebo, this.voxel_texture_atlas)

        this.canvas.oncontextmenu = (event) => { return false }
        this.input_target_element = document.body
        this.input = new Input(this.input_target_element)
        
        this.monostable_primary_mouse_button = new Monostable(false)
        this.monostable_secondary_mouse_button = new Monostable(false)
        this.limit_frame_rate_toggle = new MonostableToggle(false)
        this.pointer_lock_monostable = new Monostable(false)
        this.fullscreen_monostable = new Monostable(false)
        this._pointer_lock_requested_by_mouse = false
        
        this.key_code_map = {
            forwards: 'KeyW',
            backwards: 'KeyS',
            right: 'KeyD',
            left: 'KeyA',
            up: 'KeyF',
            down: 'Space',
            limit_framerate: 'KeyL',
            toggle_fullscreen: 'KeyB',
            toggle_pointer_lock: 'KeyM'
        }
        this.input.watch_code(this.key_code_map.forwards)
        this.input.watch_code(this.key_code_map.backwards)
        this.input.watch_code(this.key_code_map.right)
        this.input.watch_code(this.key_code_map.left)
        this.input.watch_code(this.key_code_map.up)
        this.input.watch_code(this.key_code_map.down)
        this.input.watch_code(this.key_code_map.limit_framerate)
        // injecting handlers into Input event handlers is necessary because
        // Firefox is paranoid about pointer lock and full screen requests
        this.input.watch_code_handler(
            this.key_code_map.toggle_fullscreen,
            key_status => this._toggle_fullscreen_handle(key_status)
        )
        this.input.watch_code_handler(
            this.key_code_map.toggle_pointer_lock,
            key_status => this._toggle_pointer_lock_handle(key_status)
        )
        this.input.set_mouse_down_handler(() => this._toggle_pointer_lock_mouse_handle())
        
        this.now = Timer.now()
        this.last_text_update = Timer.now()
        this.frame_counter = 0

        this.info_span.innerHTML =
            `
            <table id="info_table">
            <tr><td>Forwards</td><td>${this.key_code_map.forwards}</td></tr>
            <tr><td>Backwards</td><td>${this.key_code_map.backwards}</td></tr>
            <tr><td>Left</td><td>${this.key_code_map.left}</td></tr>
            <tr><td>Right</td><td>${this.key_code_map.right}</td></tr>
            <tr><td>Up</td><td>${this.key_code_map.up}</td></tr>
            <tr><td>Down</td><td>${this.key_code_map.down}</td></tr>
            <tr><td>Toggle fullscreen</td><td>${this.key_code_map.toggle_fullscreen}</td></tr>
            <tr><td>Toggle low framerate</td><td>${this.key_code_map.limit_framerate}</td></tr>
            <tr><td>Toggle mouse lock</td><td>${this.key_code_map.toggle_pointer_lock} or mouse click</td></tr>
            <tr><td>Add block</dt><td>primary mouse button</td></tr>
            <tr><td>Remove block</dt><td>secondary mouse button</td></tr>
            <tr><td>Switch block</dt><td>mouse wheel</td></tr>
            </table>
            `

        window.requestAnimationFrame(() => this.draw())
    }
    
    _update_tick(message) {
        this.tick_value = message.tick_value
        this.tick_time = Timer.now()
        this._send_orientation()
    }
    
    _ray_block_set(input, camera_position, camera_facing) {
        const left = input.mouse_press[0]
        const right = input.mouse_press[1]
        const ignore_queue_limit = (left || right) && input.mouse_locked
        // congestion avoidance
        if (!ignore_queue_limit && this.ray_queue_length >= cfg.RAY_QUEUE_LENGTH_LIMIT)
            return
        ++this.ray_queue_length
        
        const end = [camera_facing[0], camera_facing[1], camera_facing[2]]
        Algebra.normal_v3(end, end)
        Algebra.mul_v3_s(end, end, cfg.RAY_LENGTH)
        Algebra.add_v3(end, end, camera_position)
        
        let block_type = this.selected_block + 1
        let type = 1
        
        if (input.mouse_locked) {
            if (left) {
                type = 3
            } else if (right) {
                type = 2
                block_type = 0
            }
        }
        
        const message = {
            type,
            from: [camera_position[0], camera_position[1], camera_position[2]],
            to: end,
            block_type,
        }
        this.worker.postMessage(message)
    }
    
    
    _set_block(x, y, z, v) {
        this.worker.postMessage({ type: 4, x, y, z, v })
    }
    
    _process_ray_result(last, before) {
        Algebra.copy_v3(this.line_cube_position, last)
        --this.ray_queue_length
    }
    
    _process_message_from_worker(m) {
        const msg = m.data
        switch (msg.type) {
            case 0:
                this.voxel.update_meshes(msg.meshes)
                break
            case 1:
                this._process_ray_result(msg.last, msg.before)
                break
            case 2:
                this._set_orientation(msg)
                break
            case 3:
                this.entities.update(msg.data, this.tick_value)
                break
            case 4:
                this._update_tick(msg)
                break
            case 5:
                this.entities.add(msg.data)
                break
            case 6:
                this.entities.remove(msg.data)
                break
        }
    }


    _set_orientation(message) {
        this.initial_orientation_received = true
        this.camera.set_view([message.x, message.y, message.z], message.yaw, message.pitch)
        this.entities.set_self_id(message.id)
    }

    _toggle_fullscreen_handle(key_status) {
        this.fullscreen_monostable.update(key_status)
        if (this.fullscreen_monostable.state) {
            if (document.fullscreenElement !== null)
                document.exitFullscreen()
            else
                document.body.requestFullscreen()
        }
    }

    _toggle_pointer_lock_handle(key_status) {
        this.pointer_lock_monostable.update(key_status)
        if (this.pointer_lock_monostable.state) {
            if (document.pointerLockElement !== null)
                document.exitPointerLock()
            else
                this.canvas.requestPointerLock()
        }
    }

    _toggle_pointer_lock_mouse_handle() {
        const mouse_locked = document.pointerLockElement !== null
        if (!mouse_locked) {
            this._pointer_lock_requested_by_mouse = true
            this.canvas.requestPointerLock()
        }
    }

    _update_camera_from_input(dt, input) {
        const arrow_keys = input.arrow_keys
        
        const camera = this.camera
        const camera_rotation = [camera.rotation[0], camera.rotation[1], camera.rotation[2]]
        const up = [camera.world_up[0], camera.world_up[1], camera.world_up[2]]
        const right = [0, 0, 0]
        Algebra.mul_v3_s(camera_rotation, camera_rotation, dt * this.camera_speed)
        Algebra.mul_v3_s(up, up, dt * this.camera_speed)
        Algebra.cross_v3(right, camera_rotation, camera.world_up)
        
        const delta_movement = [0, 0, 0]
        if (arrow_keys[0] !== arrow_keys[1])
            if (arrow_keys[0])
                Algebra.add_v3(delta_movement, delta_movement, camera_rotation)
            else
                Algebra.sub_v3(delta_movement, delta_movement, camera_rotation)
        if (arrow_keys[2] !== arrow_keys[3])
            if (arrow_keys[2])
                Algebra.add_v3(delta_movement, delta_movement, right)
            else
                Algebra.sub_v3(delta_movement, delta_movement, right)
        if (arrow_keys[4] !== arrow_keys[5])
            if (arrow_keys[4])
                Algebra.add_v3(delta_movement, delta_movement, up)
            else
                Algebra.sub_v3(delta_movement, delta_movement, up)
        
        const delta_mouse_movement = input.delta_mouse_movement
    
        camera.update_view(
            delta_movement,
            delta_mouse_movement[0] * this.mouse_sensitivity,
            -delta_mouse_movement[1] * this.mouse_sensitivity
        )
    }
    
    _send_orientation() {
        if (!this.initial_orientation_received)
            return
        this.worker.postMessage({
            type: 6,
            x: this.camera.position[0],
            y: this.camera.position[1],
            z: this.camera.position[2],
            yaw: this.camera.yaw,
            pitch: this.camera.pitch
        })
    }

    draw() {
        // timing
        const time = Timer.now()
        const dt = time - this.now
        this.now = time
        // limit framerate
        this.limit_frame_rate_toggle.update(this.input.get_key_pressed_sticky(this.key_code_map.limit_framerate))
        if (this.limit_frame_rate_toggle.state)
            setTimeout(() => window.requestAnimationFrame(() => this.draw()), 200)
        else
            window.requestAnimationFrame(() => this.draw())

        const arrow_keys = [
            this.input.get_key_pressed(this.key_code_map.forwards),
            this.input.get_key_pressed(this.key_code_map.backwards),
            this.input.get_key_pressed(this.key_code_map.right),
            this.input.get_key_pressed(this.key_code_map.left),
            this.input.get_key_pressed(this.key_code_map.up),
            this.input.get_key_pressed(this.key_code_map.down),
        ]

        const wheel_step_movement = this.input.get_wheel_dy()

        this.monostable_primary_mouse_button.update(this.input.get_mouse_button(0))
        this.monostable_secondary_mouse_button.update(this.input.get_mouse_button(1))

        const mouse_press = [
            this.monostable_primary_mouse_button.state,
            this.monostable_secondary_mouse_button.state
        ]

        const mouse_locked = document.pointerLockElement !== null
        if (!mouse_locked) {
            mouse_press[0] = false
            mouse_press[1] = false
        }
        if (this._pointer_lock_requested_by_mouse) {
            this._pointer_lock_requested_by_mouse = false
            mouse_press[0] = false
            mouse_press[1] = false
        }

        const delta_mouse_movement = this.input.get_mouse_movement()
        if (!mouse_locked) {
            delta_mouse_movement[0] = 0
            delta_mouse_movement[1] = 0
        }

        /** @type {ViewPort} */
        const viewport_voxel = {
            x: 0,
            y: 0,
            width: this.canvas.width,
            height: this.canvas.height
        }
        const gl = this.gl
        gl.clearColor(0, 0, 0.2, 1)
        gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT)
        const valid_block_range = [0, Math.floor(this.assets.generated_transp.length) - 1]
        // TODO: do not redraw if nothing has changed to save power
        const t_since_tick = time - this.tick_time
        const inputt = { arrow_keys, delta_mouse_movement, mouse_locked, wheel_step_movement, mouse_press }
        this._update_camera_from_input(dt, inputt)
        const aspectt = this.canvas.width / this.canvas.height
        this.camera.update_aspect(aspectt)
        const VP = this.camera.view_projection
        const camera_position = this.camera.position
        const camera_facing = this.camera.facing
        this._ray_block_set(inputt, camera_position, camera_facing)
        const VP_and_translation = this.camera.compute_and_get_view_projection_and_int_translation()
        const [voxel_meshes_rendered, voxel_meshes_skipped, element_count_rendered] = this.voxel.draw(viewport_voxel, VP_and_translation.view_projection, VP_and_translation.int_translation)
        const ls = this.line_shader
        gl.useProgram(ls.program)
        gl.uniformMatrix4fv(ls.uniforms.u_MVP, false, VP)
        this.line_square.draw(gl, this.line_cube_position, ls.uniforms.u_offset, ls.uniforms.u_color, ls.attributes.a_position)

        this.entities.draw(this.gl, t_since_tick, VP, this.tick_value)
        const rotating_block_window_size = Math.min(Math.floor(viewport_voxel.width / 4), Math.floor(viewport_voxel.height / 4))
        /** @type {ViewPort} */
        const viewport_block = {
            x: 0,
            y: 0,
            width: rotating_block_window_size,
            height: rotating_block_window_size,
        }
        
        this.selected_block -= inputt.wheel_step_movement
        this.selected_block = Math.max(Math.min(this.selected_block, valid_block_range[1] - 1), valid_block_range[0])
        this.gui.draw(this.gl, this.line_shader, this.selected_block + 1, viewport_block, viewport_voxel)

        ++this.frame_counter
        const dt_last_update = time - this.last_text_update
        console.assert(this.voxel.get_mesh_count() === voxel_meshes_rendered + voxel_meshes_skipped, 'Some meshes disappeared somewhere.')
        if (dt_last_update >= 1 / cfg.TEXTUPDATES_PER_SECOND) {
            const p = camera_position
            this.last_text_update = time
            const FPS = Math.round(this.frame_counter / dt_last_update)
            const text = `
                Position: (${p[0].toFixed(1)}, ${p[1].toFixed(1)}, ${p[2].toFixed(1)})<br />
                ${FPS} FPS<br />
                Selected block: ${this.selected_block + 1}<br />
                Loaded voxel mesh count: ${this.voxel.get_mesh_count()}<br />
                Rendered voxel mesh count: ${voxel_meshes_rendered}<br />
                Skipped voxel mesh count: ${voxel_meshes_skipped}<br />
                Quads rendered: ${element_count_rendered / 6}
            `
            this.status_span.innerHTML = text
            this.frame_counter = 0
        }
    }
}
