import init_run from './Main.js'
import init_assets_gen from './AssetsGen.js'

/**
 * App entry.
 */
window.addEventListener('DOMContentLoaded', event => {
    window['templates'] = {}
    for (const template of document.querySelectorAll('template')) {
        if (template.id === '')
            continue
        const template_clone = document.importNode(template.content, true)
        window.templates[template.id] = template_clone
        template.remove()
    }
    set_login()
})

async function set_run(name, url) {
    const template_clone = document.importNode(window.templates.run, true)
    document.body.innerHTML = ''
    document.body.appendChild(template_clone)
    await init_run(name, url)
}

function set_stop(name, url) {
    const template_clone = document.importNode(window.templates.stop, true)
    document.body.innerHTML = ''
    document.body.appendChild(template_clone)
    stop_server(name, url)
}

function set_login() {
    const template_clone = document.importNode(window.templates.login, true)
    template_clone.querySelector('#join_server_button').addEventListener('click', event => {
        const url = document.querySelector('#url').value
        const name = document.querySelector('#name').value
        set_run(name, url)
    })
    template_clone.querySelector('#stop_server_button').addEventListener('click', event => {
        const url = document.querySelector('#url').value
        const name = document.querySelector('#name').value
        set_stop(name, url)
    })
    template_clone.querySelector('a').addEventListener('click', event => {
        set_assets_gen()
    })
    document.body.innerHTML = ''
    document.body.appendChild(template_clone)
}

async function set_assets_gen() {
    const template_clone = document.importNode(window.templates.assets_gen, true)
    document.body.innerHTML = ''
    document.body.appendChild(template_clone)
    await init_assets_gen()
}

function stop_server(name, url) {
    const ws = new WebSocket(url)
    ws.binaryType = 'arraybuffer'
    ws.onopen = () => {
        const message = new DataView(new ArrayBuffer(17))
        message.setUint8(0, 9)
        // user name
        for (let i = 0; i < 16; ++i) {
            message.setInt8(i + 1, 0)
        }
        for (let i = 0; i < Math.min(16, name.length); ++i) {
            const c = name.charCodeAt(i)
            message.setInt8(i + 1, c)
        }
        ws.send(message)
        // stop message
        const message2 = new DataView(new ArrayBuffer(5))
        message2.setUint8(0, 6)
        message2.setUint8(1, 115)
        message2.setUint8(2, 116)
        message2.setUint8(3, 111)
        message2.setUint8(4, 112)
        ws.send(message2)
        document.querySelector("p").innerHTML = "Close request sent."
    }
}
