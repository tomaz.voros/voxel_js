import ShaderProgram from "./ShaderProgram.js"
import QuadEBO from "./QuadEBO.js"

// ISSUE(0)
// import Algebra from './Algebra.js'

class BlockMeshes {
    /**
     * @param {WebGL2RenderingContext} gl
     * @param {Assets} assets
     * @param {QuadEBO} ebo
     * @param {WebGLTexture} atlas_texture
     */
    constructor(gl, assets, ebo, atlas_texture) {
        this._atlas_texture = atlas_texture
        const texture_positions = new Float32Array(assets.generated_coords)

        this._shader = new ShaderProgram(gl,
            [ { type: gl.VERTEX_SHADER, source: assets.block_mesh_vert },
            { type: gl.FRAGMENT_SHADER, source: assets.block_mesh_frag } ]
        )

        if (texture_positions.length % 48 !== 0)
            throw new Error('Invalid texture_posirions')

        const cube_vertices = new Int8Array([
            -1,-1,-1,-1,-1, 1,-1, 1, 1,-1, 1,-1,
             1,-1, 1, 1,-1,-1, 1, 1,-1, 1, 1, 1,
             1,-1, 1,-1,-1, 1,-1,-1,-1, 1,-1,-1,
            -1, 1, 1, 1, 1, 1, 1, 1,-1,-1, 1,-1,
             1,-1,-1,-1,-1,-1,-1, 1,-1, 1, 1,-1,
            -1,-1, 1, 1,-1, 1, 1, 1, 1,-1, 1, 1,
        ])

        this._ebo = ebo

        const vertices = []
        this._block_begin_end = []
        let element_i = 0
        // currently only regular full blocks supported
        for (let i = 0; i < texture_positions.length; i += 48) {
            const this_begin = element_i
            for (let side = 0; side < 6; ++side) {
                for (let vert_i = 0; vert_i < 4; ++vert_i) {
                    vertices.push(
                        [
                            cube_vertices[side * 12 + vert_i * 3 + 0],
                            cube_vertices[side * 12 + vert_i * 3 + 1],
                            cube_vertices[side * 12 + vert_i * 3 + 2],
                            texture_positions[i + side * 8 + vert_i * 2 + 0],
                            texture_positions[i + side * 8 + vert_i * 2 + 1]
                        ]
                    )
                }
                element_i += 6
            }
            const this_end = element_i
            this._block_begin_end.push([this_begin, this_end])
        }

        // assert: QuadEBO is large enough
        const vertices_float = new Float32Array(vertices.flat())
        this._delete_me = vertices_float

        this._vbo_vertices = gl.createBuffer()
        gl.bindBuffer(gl.ARRAY_BUFFER, this._vbo_vertices)
        gl.bufferData(gl.ARRAY_BUFFER, vertices_float, gl.STATIC_DRAW)

        this._vao = gl.createVertexArray()
        gl.bindVertexArray(this._vao)
        gl.bindBuffer(gl.ARRAY_BUFFER, this._vbo_vertices)
        gl.vertexAttribPointer(this._shader.attributes.a_position, 3, gl.FLOAT, gl.FALSE, 5 * vertices_float.BYTES_PER_ELEMENT, 0)
        gl.enableVertexAttribArray(this._shader.attributes.a_position)
        gl.vertexAttribPointer(this._shader.attributes.a_texture_position, 2, gl.FLOAT, gl.FALSE, 5 * vertices_float.BYTES_PER_ELEMENT, 3 * vertices_float.BYTES_PER_ELEMENT)
        gl.enableVertexAttribArray(this._shader.attributes.a_texture_position)

        gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, this._ebo.ebo)
        gl.bindVertexArray(null)
    }

    draw_test(gl, selected_index_test, viewport_block) {
        const aspect = viewport_block.width / viewport_block.height
        const model = Algebra.m4()
        const view = Algebra.m4()
        const projection = Algebra.m4()
        const view_projection = Algebra.m4()
        const model_view_projection = Algebra.m4()

        // TODO: use the same time as in main/rest for that frame
        Algebra.rotation_y_m4(model, Timer.now() / 2.5)
        Algebra.view_m4(view, [0, 0, -7.5], [0, 0, 0], [0, 1, 0])
        Algebra.perspective_m4(projection, 1, aspect, 0.1, 10)

        Algebra.mul_m4(view_projection, projection, view)
        Algebra.mul_m4(model_view_projection, view_projection, model)

        gl.viewport(viewport_block.x, viewport_block.y, viewport_block.width, viewport_block.height)
        gl.clearColor(0, 0.1, 0.1, 1)
        gl.scissor(viewport_block.x, viewport_block.y, viewport_block.width, viewport_block.height)
        gl.enable(gl.SCISSOR_TEST)
        gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT)
        gl.disable(gl.SCISSOR_TEST)

        gl.useProgram(this._shader.program)
        gl.disable(gl.DEPTH_TEST)
        gl.uniformMatrix4fv(this._shader.uniforms.u_MVP, false, model_view_projection)
        gl.uniform1i(this._shader.uniforms.u_texture, 0)
        gl.activeTexture(gl.TEXTURE0)
        gl.bindTexture(gl.TEXTURE_2D, this._atlas_texture)
        gl.bindVertexArray(this._vao)
        if (false) {
            for (const block of this._block_begin_end) {
                gl.drawElements(gl.TRIANGLES, block[1] - block[0], this._ebo.index_type(gl), block[0])
            }
        } else {
            // TODO: draw only existing

            const index = Math.max(Math.min(selected_index_test, this._block_begin_end.length - 1), 0)
            if (this._block_begin_end.length > 0) {
                const block = this._block_begin_end[index]
                gl.drawElements(gl.TRIANGLES, block[1] - block[0], this._ebo.index_type(gl), block[0]*this._ebo.index_size())
            }
        }

        gl.bindVertexArray(null)
    }
}

export default class GUI {
  constructor(gl, assets, quad_ebo, atlas_texture) {
    this._block_meshes = new BlockMeshes(gl, assets, quad_ebo, atlas_texture)

    const cs = 0.03
    const cross_mesh = [
      -cs,   0, 0,  cs,  0, 0,
        0, -cs, 0,   0, cs, 0,
    ]
    this._cross_vbo = gl.createBuffer()
    gl.bindBuffer(gl.ARRAY_BUFFER, this._cross_vbo)
    gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(cross_mesh), gl.STATIC_DRAW)

    const square_line_mesh = [
      -1/16,-1/16, 0, 1/16,-1/16, 0,
       1/16, 1/16, 0,-1/16, 1/16, 0,
    ]
    this._square_line_vbo = gl.createBuffer()
    gl.bindBuffer(gl.ARRAY_BUFFER, this._square_line_vbo)
    gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(square_line_mesh), gl.STATIC_DRAW)

    this._settings_on = false

    const square_mesh = [
      -1,-1,-0.1, 0,0, 0,  0, 1,1,1,1,
       1,-1,-0.1, 1,0, 1, 0, 1,1,1,1,
       1, 1,-0.1, 1,1, 1,1, 1,1,1,1,

       1, 1,-0.1, 1,1, 1,1, 1,1,1,1,
      -1, 1,-0.1, 0,1,  0,1, 1,1,1,1,
      -1,-1,-0.1, 0,0,  0, 0, 1,1,1,1,
    ]
    this._plate_size = 0.4
    this._plate_offset = -1.45
    this._plate_vbo = gl.createBuffer()
    gl.bindBuffer(gl.ARRAY_BUFFER, this._plate_vbo)
    gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(square_mesh), gl.STATIC_DRAW)
  }

  get is_on() {
    return this._settings_on
  }

  toggle_settings() {
    if (this._settings_on) {
      this._settings_on = false
    } else {
      this._settings_on = true
    }
  }

  draw(gl, shader, block_selected, viewport_block, viewport_full) {
    const aspect = viewport_full.width / viewport_full.height
    gl.viewport(viewport_full.x, viewport_full.y, viewport_full.width, viewport_full.height)
    gl.useProgram(shader.program)
    const l_transformation_matrix = Algebra.m4()
    let s = 2
    if (aspect > 1) s *= aspect
    Algebra.ortho_m4(l_transformation_matrix, s, aspect, -1, 1)
    const l_attribute_position = shader.attributes.a_position
    const l_uniform_offset = shader.uniforms.u_offset
    const l_uniform_color = shader.uniforms.u_color
    const l_uniform_MVP = shader.uniforms.u_MVP
    gl.bindBuffer(gl.ARRAY_BUFFER, this._cross_vbo)
    gl.enableVertexAttribArray(l_attribute_position)
    gl.vertexAttribPointer(l_attribute_position, 3, gl.FLOAT, false, 4 * 3, 0)
    gl.uniform3f(l_uniform_offset, 0, 0, 0)
    gl.uniform4f(l_uniform_color, 1, 1, 1, 1)
    gl.uniformMatrix4fv(l_uniform_MVP, false, l_transformation_matrix)
    gl.drawArrays(gl.LINES, 0, 4)
    gl.disableVertexAttribArray(l_attribute_position)

    if (this._settings_on) {
      /*
      const attribute_position = square_shader.attributes.a_position
      const attribute_ao_position = square_shader.attributes.a_ao_position
      const attribute_texture_position = square_shader.attributes.a_texture_position
      const attribute_ambient_occlusion = square_shader.attributes.a_ambient_occlusion
      const uniform_offset = square_shader.uniforms.u_offset
      const uniform_MVP = square_shader.uniforms.u_MVP
      const uniform_texture = square_shader.uniforms.u_texture
      gl.useProgram(square_shader.program)
      const translation = Algebra.m4()
      Algebra.identity_m4(translation)
      //console.log(aspect)
      Algebra.translation_m4(translation, [this._plate_offset, this._plate_offset, 0])
      const transformation_matrix = Algebra.m4()
      let s = 2 / this._plate_size
      if (aspect > 1) s *= aspect
      Algebra.ortho_m4(transformation_matrix, s, aspect, -1, 1)
      Algebra.mul_m4(transformation_matrix, transformation_matrix, translation)
      gl.bindBuffer(gl.ARRAY_BUFFER, this._plate_vbo)
      gl.enableVertexAttribArray(attribute_position)
      gl.enableVertexAttribArray(attribute_ao_position)
      gl.enableVertexAttribArray(attribute_texture_position)
      gl.enableVertexAttribArray(attribute_ambient_occlusion)

      gl.vertexAttribPointer(attribute_position, 3, gl.FLOAT, false, 4 * 11, 4 * 0)
      gl.vertexAttribPointer(attribute_ao_position, 2, gl.FLOAT, false, 4 * 11, 4 * 3)
      gl.vertexAttribPointer(attribute_texture_position, 2, gl.FLOAT, false, 4 * 11, 4 * 5)
      gl.vertexAttribPointer(attribute_ambient_occlusion, 4, gl.FLOAT, false, 4 * 11, 4 * 7)

      gl.uniform3f(uniform_offset, 0, 0, 0)
      gl.uniformMatrix4fv(uniform_MVP, false, transformation_matrix)
      gl.uniform1i(uniform_texture, 0) // assume texture already bound
      gl.drawArrays(gl.TRIANGLES, 0, 6)

      gl.disableVertexAttribArray(attribute_position)
      gl.disableVertexAttribArray(attribute_ao_position)
      gl.disableVertexAttribArray(attribute_texture_position)
      gl.disableVertexAttribArray(attribute_ambient_occlusion)

      // square line
      gl.useProgram(shader.program)
      gl.bindBuffer(gl.ARRAY_BUFFER, this._square_line_vbo)
      gl.enableVertexAttribArray(l_attribute_position)
      gl.vertexAttribPointer(l_attribute_position, 3, gl.FLOAT, false, 4 * 3, 0)
      const offset_x = 2/16 * (block_selected % 16)
      const offset_y = 2/16 * Math.floor((block_selected / 16))
      gl.uniform3f(l_uniform_offset, 1/16-1 + offset_x, 1/16-1 + offset_y, 0)
      gl.uniform4f(l_uniform_color, 1, 1, 1, 1)
      //Algebra.ortho_m4(l_transformation_matrix, (2*16)/this._plate_size, aspect, -1, 1)
      gl.uniformMatrix4fv(l_uniform_MVP, false, transformation_matrix)
      gl.drawArrays(gl.LINE_LOOP, 0, 4)
      gl.disableVertexAttribArray(l_attribute_position)
      */
    }

    if (true || this._settings_on) {
        this._block_meshes.draw_test(gl, block_selected, viewport_block)
    }
  }
}
