// ISSUE(0)
// import Utility from './Utility.js'

export default class Assets {
    static async load() {
        const assets = new Object()
        
        const value = await Promise.all([
            Utility.load_text('shader/voxel.vert'),
            Utility.load_text('shader/voxel.frag'),
            Utility.load_text('shader/line.vert'),
            Utility.load_text('shader/line.frag'),
            Utility.load_text('shader/entity.vert'),
            Utility.load_text('shader/entity.frag'),
            Utility.load_json('assets/assets.json'),
            Utility.load_text('shader/block_mesh.vert'),
            Utility.load_text('shader/block_mesh.frag'),
        ])
        
        const name = [
            'voxel_vert',
            'voxel_frag',
            'line_vert',
            'line_frag',
            'entity_vert',
            'entity_frag',
            'assets',
            'block_mesh_vert',
            'block_mesh_frag',
        ]
        
        for (let i = 0; i < name.length; ++i)
            assets[name[i]] = value[i]
        
        return assets
    }
    
    static generate_atlas_data(atlas_json) {
        const tiles = new Map()
        const tiles_per_row = atlas_json.atlas.tiles_per_row
        if (!Utility.is_power_of_2(atlas_json.atlas.tile_width))
            throw new Error('Tile width is not power of two.')
        if (tiles_per_row < 1 || tiles_per_row > 8 || !Utility.is_power_of_2(tiles_per_row))
            throw new Error('Unsupported assets.atlas.tiles_per_row value.')
        const tile_width = 1 << (7 - Math.log2(tiles_per_row))
        let x = 0
        let y = tiles_per_row - 1
        for (const tile of atlas_json.tiles)
            for (const file of tile.sides)
                if (!tiles.has(file.file_name)) {
                    if (y === -1)
                        throw new Error('Too many tiles')
                    tiles.set(file.file_name, [x, y])
                    ++x
                    if (x === tiles_per_row) {
                        x = 0
                        --y
                    }
                }
        const coord_lookup = [0, 0, 1, 0, 1, 1, 0, 1]
        const mirror = (i, type) => {
            if (type === true)
                return [i[6], i[7], i[4], i[5], i[2], i[3], i[0], i[1]]
            else
                return [i[0], i[1], i[2], i[3], i[4], i[5], i[6], i[7]]
        }
        const rotate = (i, rotation) => {
            if (rotation === 1)
                return [i[2], i[3], i[4], i[5], i[6], i[7], i[0], i[1]]
            else if (rotation === 2)
                return [i[4], i[5], i[6], i[7], i[0], i[1], i[2], i[3]]
            else if (rotation === 3)
                return [i[6], i[7], i[0], i[1], i[2], i[3], i[4], i[5]]
            else
                return [i[0], i[1], i[2], i[3], i[4], i[5], i[6], i[7]]
        }
        const scale = (i, factor) => {
            return [
                i[0] * factor, i[1] * factor, i[2] * factor, i[3] * factor,
                i[4] * factor, i[5] * factor, i[6] * factor, i[7] * factor,
            ]
        }
        const translate = (i, offset_x, offset_y) => {
            return [
                i[0] + offset_x, i[1] + offset_y, i[2] + offset_x, i[3] + offset_y,
                i[4] + offset_x, i[5] + offset_y, i[6] + offset_x, i[7] + offset_y,
            ]
        }
        const coords = []
        const transparent = []
        for (const tile of atlas_json.tiles) {
            for (let side = 0; side < 6; ++side) {
                const side_val = tile.sides[Math.min(tile.sides.length - 1, side)]
                const do_rotate = 'rotate' in side_val ? side_val.rotate : 0
                const do_mirror = 'mirror' in side_val ? side_val.mirror : false
                const offset = tiles.get(side_val.file_name)
                const position = translate(
                    scale(
                        rotate(mirror(coord_lookup, do_mirror), do_rotate),
                        tile_width
                    ),
                    offset[0] * tile_width,
                    offset[1] * tile_width
                );
                for (let i = 0; i < 8; ++i)
                    coords[tile.index * 6 * 4 * 2 + side * 4 * 2 + i] = position[i]
            }
            transparent[tile.index] = 'transparent' in tile ? tile.transparent : false
        }
        for (let i = 0; i < transparent.length; ++i)
            if (typeof transparent[i] !== "boolean")
                transparent[i] = false
        for (let i = 0; i < coords.length; ++i)
            if (typeof coords[i] !== 'number')
                coords[i] = 0
        
        const file_prefix = atlas_json.file_prefix
        const tile_array = Array.from(tiles, tile => file_prefix + tile[0])
        return {
            coords,
            transparent,
            tiles: tile_array,
            tiles_per_row,
            tile_width: atlas_json.atlas.tile_width
        }
    }

    /**
     * @param {Array<String>} tiles
     * @param {number} tiles_per_row
     * @param {number} tile_width
     */
    static async generate_atlas(tiles, tiles_per_row, tile_width) {
        if (tiles.length > tile_width * tile_width)
            throw new Error('Too many tiles.')
        /** @type {ImageData[]} */
        const tiles_data = await Promise.all(
            tiles.map(tile => Utility.load_image(tile))
        )
        for (const tile of tiles_data)
            if (tile.width !== tile_width || tile.height !== tile_width)
                throw new Error('An image has wrong dimensions.')
        
        const canvas = document.createElement('canvas')
        const context = canvas.getContext('2d')
        canvas.width = tiles_per_row * tile_width
        canvas.height = tiles_per_row * tile_width
        context.globalCompositeOperation = 'copy'
        
        let done = false
        for (let y = 0; y < tiles_per_row; ++y) {
            for (let x = 0; x < tiles_per_row; ++x) {
                const index = y * tiles_per_row + x
                if (tiles_data.length <= index) {
                    done = true
                    break
                }
                context.putImageData(
                    tiles_data[index],
                    x * tile_width,
                    y * tile_width
                )
            }
            if (done)
                break
        }
        

        return context.getImageData(0, 0, canvas.width, canvas.height)
    }
}
