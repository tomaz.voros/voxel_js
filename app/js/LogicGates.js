export class Monostable {
    /**
     * @param {boolean} starting_state Initial value.
     */
    constructor(starting_state) {
        /**
         * 0 ... not set
         * 1 ... set and not read
         * 2 ... set and read
         * @type {number}
         */
        this._state = starting_state ? 1 : 0
    }
    /**
     * @param {boolean} input Input value.
     */
    update(input) {
        if (input) {
            if (this._state === 0)
                this._state = 1
        } else {
            this._state = 0
        }
    }
    /**
     * @returns {boolean} Output state.
     */
    get state() {
        if (this._state === 1) {
            this._state = 2
            return true
        } else {
            return false
        }
    }
}

export class Toggle {
    /**
     * @param {boolean} starting_state Initial state.
     */
    constructor(starting_state) {
        this._state = starting_state
    }
    /**
     * Switches state.
     */
    toggle() {
        this._state = !this._state
    }
    /**
     * @param {boolean} do_toggle Only toggle if true.
     */
    toggle_if(do_toggle) {
        this._state = do_toggle !== this._state
    }
    /**
     * @returns {boolean} Current state.
     */
    get state() {
        return this._state
    }
}

export class MonostableToggle {
    /**
     * @param {boolean} starting_state Initial value.
     */
    constructor(starting_state) {
        this._monstable = new Monostable(false)
        this._toggle = new Toggle(starting_state)
    }
    /**
     * @param {boolean} input Input value.
     */
    update(input) {
        this._monstable.update(input)
        this._toggle.toggle_if(this._monstable.state)
    }
    /**
     * @returns {boolean} Output state.
     */
    get state() {
        return this._toggle.state
    }
}
