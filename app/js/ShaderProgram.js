/**
 * @typedef {Object} ShaderData
 * @property {number} type Type of shader.
 * @property {String} source Shader source text.
 */

export default class ShaderProgram {
    /**
     * @param {WebGL2RenderingContext} gl
     * @param {Array<ShaderData>} shaders_data
     */
    constructor(gl, shaders_data) {
        /** @type {WebGLProgram} */
        this.program = gl.createProgram()
        /** @type {Object<String, number>} */
        this.attributes = new Object()
        /** @type {Object<String, WebGLUniformLocation>} */
        this.uniforms = new Object()

        const shader_list = []
        for (const shader_data of shaders_data) {
            const shader = gl.createShader(shader_data.type)
            shader_list.push(shader)
            gl.shaderSource(shader, shader_data.source)
            gl.compileShader(shader)
            if (!gl.getShaderParameter(shader, gl.COMPILE_STATUS)) {
                const error_message = `Error compiling shader: ${gl.getShaderInfoLog(shader)}`
                for (const shader_i of shader_list)
                    gl.deleteShader(shader_i)
                gl.deleteProgram(this.program)
                throw new Error(error_message)
            }
        }

        for (const shader of shader_list)
            gl.attachShader(this.program, shader)
        gl.linkProgram(this.program)
        for (const shader of shader_list) {
            gl.detachShader(this.program, shader)
            gl.deleteShader(shader)
        }

        if (!gl.getProgramParameter(this.program, gl.LINK_STATUS)) {
            const error_message = `Error linking program: ${gl.getProgramInfoLog(this.program)}`
            gl.deleteProgram(this.program)
            throw new Error(error_message)
        }

        const attribute_count = gl.getProgramParameter(this.program, gl.ACTIVE_ATTRIBUTES)
        for (let i = 0; i < attribute_count; ++i) {
          const attribute = gl.getActiveAttrib(this.program, i)
          this.attributes[attribute.name] = gl.getAttribLocation(this.program, attribute.name)
        }

        const uniform_count = gl.getProgramParameter(this.program, gl.ACTIVE_UNIFORMS)
        for (let i = 0; i < uniform_count; ++i) {
          const uniform = gl.getActiveUniform(this.program, i)
          this.uniforms[uniform.name] = gl.getUniformLocation(this.program, uniform.name)
        }
    }
}
