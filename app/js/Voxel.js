import ShaderProgram from './ShaderProgram.js'
// ISSUE(0)
// import Algebra from './Algebra.js'
// import cfg from './cfg.js'

/**
 * @typedef {Object} Mesh
 * @property {number} off_x
 * @property {number} off_y
 * @property {number} off_z
 * @property {number} element_count
 * @property {WebGLVertexArrayObject} vao
 * @property {WebGLBuffer} vbo
 */

export default class Voxel {
    constructor(gl, assets, quad_ebo, voxel_texture_atlas) {
        this._gl = gl
        /** @type {Map<number, Mesh>} */
        this._meshes = new Map()
        /** @type {Array<WebGLVertexArrayObject>} */
        this._vao_cache = new Array()
        /** @type {Array<WebGLBuffer>} */
        this._vbo_cache = new Array()
        
        this._voxel_shader = new ShaderProgram(
            gl, [ { type: gl.VERTEX_SHADER, source: assets.voxel_vert },
            { type: gl.FRAGMENT_SHADER, source: assets.voxel_frag } ]
        )
            
        this._voxel_atlas = voxel_texture_atlas
        this._quad_ebo = quad_ebo
    }

    _get_vao_vbo() {
        console.assert(this._vbo_cache.length === this._vao_cache.length, 'Broken VAO, VBO cache.')
        if (this._vbo_cache.length > 0) {
            return [this._vao_cache.pop(), this._vbo_cache.pop()]
        } else {
            return [this._gl.createVertexArray(), this._gl.createBuffer()]
        }
    }

    /**
     * 
     * @param {WebGLVertexArrayObject} vao To be returned to cache or scrapped.
     * @param {WebGLBuffer} vbo To be returned to cache or scrapped.
     */
    _return_vao_vbo(vao, vbo) {
        console.assert(this._vbo_cache.length === this._vao_cache.length, 'Broken VAO, VBO cache.')
        if (this._vbo_cache.length >= cfg.MAX_VAO_VBO_CACHE_SIZE) {
            this._gl.deleteVertexArray(vao)
            this._gl.deleteBuffer(vbo)
            return [this._vao_cache.pop(), this._vbo_cache.pop()]
        } else {
            this._gl.bindBuffer(this._gl.ARRAY_BUFFER, vbo)
            this._gl.bufferData(this._gl.ARRAY_BUFFER, 0, this._gl.STATIC_DRAW)
            this._vao_cache.push(vao)
            this._vbo_cache.push(vbo)
        }
    }

    update_meshes(meshes) {
        for (const mesh of meshes)
            this._update_mesh(mesh.position, mesh.buffer, mesh.element_count)
    }
    
    _update_mesh(mesh_position, buffer, element_count) {
        const gl = this._gl
        const key = Utility.position_to_key(mesh_position)
        const mesh = this._meshes.get(key)
        const offset = Algebra.mul_v3(Algebra.v3(), mesh_position, cfg.MESH_SIZE)
        console.assert(element_count === 0 ? mesh !== undefined : true, 'Requested removing non-existing mesh.')
        if (element_count > 0 && mesh === undefined) {
            // add mesh
            const attributes = this._voxel_shader.attributes
            const [vao, vbo] = this._get_vao_vbo()
            gl.bindVertexArray(vao)
            gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, this._quad_ebo.ebo)
            gl.bindBuffer(gl.ARRAY_BUFFER, vbo)
            gl.vertexAttribPointer(attributes.a_position, 3, gl.UNSIGNED_BYTE, gl.FALSE, 6, 0)
            gl.vertexAttribIPointer(attributes.a_ambient_occlusion, 1, gl.UNSIGNED_BYTE, 6, 3)
            gl.vertexAttribPointer(attributes.a_texture_position, 2, gl.UNSIGNED_BYTE, gl.FALSE, 6, 4)
            gl.enableVertexAttribArray(attributes.a_position)
            gl.enableVertexAttribArray(attributes.a_ambient_occlusion)
            gl.enableVertexAttribArray(attributes.a_texture_position)
            gl.bufferData(gl.ARRAY_BUFFER, buffer, gl.STATIC_DRAW)
            gl.bindVertexArray(null) // TODO: fix other code so stat this is not necessary
            this._meshes.set(
                key,
                { off_x: offset[0], off_y: offset[1], off_z: offset[2],
                  element_count, vao, vbo }
            )
        } else if (element_count > 0 && mesh !== undefined) {
            // update mesh
            gl.bindBuffer(gl.ARRAY_BUFFER, mesh.vbo)
            gl.bufferData(gl.ARRAY_BUFFER, buffer, gl.STATIC_DRAW)
            mesh.element_count = element_count
            mesh.off_x = offset[0]
            mesh.off_y = offset[1]
            mesh.off_z = offset[2]
        } else if (element_count === 0 && mesh !== undefined) {
            // remove mesh
            this._return_vao_vbo(mesh.vao, mesh.vbo)
            const exists = this._meshes.delete(key)
            console.assert(exists)
        }
    }

    get_mesh_count() {
        return this._meshes.size
    }
    
    /**
     * @returns [meshes_rendered, meshes_skipped, element_count_rendered]
     */
    draw(viewport, VP, camera_int_offset) {
        const gl = this._gl
        gl.viewport(viewport.x, viewport.y, viewport.width, viewport.height)
        gl.enable(gl.CULL_FACE)
        gl.enable(gl.DEPTH_TEST)
        
        const shader = this._voxel_shader
        const uniform_MVP = shader.uniforms.u_MVP
        const uniform_offset = shader.uniforms.u_offset
        const uniform_texture = shader.uniforms.u_texture
        
        gl.useProgram(shader.program)
        gl.uniformMatrix4fv(uniform_MVP, false, VP)
        gl.uniform1i(uniform_texture, 0)
        gl.activeTexture(gl.TEXTURE0)
        gl.bindTexture(gl.TEXTURE_2D, this._voxel_atlas)

        const index_type = this._quad_ebo.index_type(gl)
        console.assert(cfg.MESH_OFFSET[0] === cfg.MESH_OFFSET[1] && cfg.MESH_OFFSET[0] === cfg.MESH_OFFSET[2])
        console.assert(cfg.MESH_SIZE[0] === cfg.MESH_SIZE[1] && cfg.MESH_SIZE[0] === cfg.MESH_SIZE[2])
        const mesh_offset_to_center = [
            // those must be float operations if cfg.MESH_SIZE[x] is not even (info for non JavaScript implementations)
            cfg.MESH_OFFSET[0] + cfg.MESH_SIZE[0] / 2 - camera_int_offset[0],
            cfg.MESH_OFFSET[1] + cfg.MESH_SIZE[1] / 2 - camera_int_offset[1],
            cfg.MESH_OFFSET[2] + cfg.MESH_SIZE[2] / 2 - camera_int_offset[2],
        ]

        const mesh_side_length = cfg.MESH_SIZE[0] + 1
        // those must be float operations if mesh_side_length is not even (info for non JavaScript implementations)
        const mesh_radius = Math.sqrt(3 * (mesh_side_length / 2) ** 2)

        const frustum_planes = new Array(16)
        Algebra.matrix_to_normalized_frustum_planes(frustum_planes, VP)

        let meshes_rendered = 0
        let meshes_skipped = 0
        let element_count_rendered = 0

        // for reuse because JavaScript is slow
        const mesh_off = Algebra.v3()
        const mesh_center = Algebra.v3()
        const mesh_off_adjusted = Algebra.v3()

        for (const mesh of this._meshes.values()) {
            mesh_off[0] = mesh.off_x
            mesh_off[1] = mesh.off_y
            mesh_off[2] = mesh.off_z
            Algebra.add_v3(mesh_center, mesh_off, mesh_offset_to_center)

            if (!Algebra.sphere_in_frustum(frustum_planes, mesh_center, mesh_radius)) {
                ++meshes_skipped
                continue
            } else {
                element_count_rendered += mesh.element_count
                ++meshes_rendered
            }

            Algebra.sub_v3(mesh_off_adjusted, mesh_off, camera_int_offset)
            gl.bindVertexArray(mesh.vao)
            gl.uniform3f(uniform_offset, mesh_off_adjusted[0], mesh_off_adjusted[1], mesh_off_adjusted[2])
            gl.drawElements(gl.TRIANGLES, mesh.element_count, index_type, 0)
        }
        
        gl.bindVertexArray(null) // TODO: fix other code so stat this is not necessary
        return [meshes_rendered, meshes_skipped, element_count_rendered]
    }
}
