export default class QuadEBO {
  constructor(gl) {
    this._max_vertices = 16000 * 4 // WebGL 1 is limited to uint16 indices (52224 should be enough for 16^3 optimised mesh)
    const indices = []

    for (let i = 0, j = 0; j < this._max_vertices; j += 4) {
      indices[i++] = j + 0
      indices[i++] = j + 1
      indices[i++] = j + 2
      indices[i++] = j + 2
      indices[i++] = j + 3
      indices[i++] = j + 0
    }

    this._ebo = gl.createBuffer()
    gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, this._ebo)
    gl.bufferData(gl.ELEMENT_ARRAY_BUFFER, new Uint16Array(indices), gl.STATIC_DRAW)
  }

  get max_vertices() {
    return this._max_vertices
  }

  get ebo() {
    return this._ebo
  }

  index_type(gl) {
    return gl.UNSIGNED_SHORT
  }

  index_size(gl) {
    return Uint16Array.BYTES_PER_ELEMENT
  }
}
