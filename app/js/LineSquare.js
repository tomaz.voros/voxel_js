export default class LineSquare {
  constructor(gl) {
    const over = 0.02
    const square_line_mesh = [
      0-over,0-over,0-over,
      0-over,0-over,1+over,
      0-over,1+over,0-over,
      0-over,1+over,1+over,
      1+over,0-over,0-over,
      1+over,0-over,1+over,
      1+over,1+over,0-over,
      1+over,1+over,1+over
    ]
    const square_line_elements = [
      0,1, 1,3, 3,2, 2,0,
      4,5, 5,7, 7,6, 6,4,
      0,4, 1,5, 2,6, 3,7,
    ]
    this._vbo = gl.createBuffer()
    this._ebo = gl.createBuffer()
    gl.bindBuffer(gl.ARRAY_BUFFER, this._vbo)
    gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(square_line_mesh), gl.STATIC_DRAW)
    gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, this._ebo)
    gl.bufferData(gl.ELEMENT_ARRAY_BUFFER, new Uint16Array(square_line_elements), gl.STATIC_DRAW)
    this._element_count = square_line_elements.length
  }

  draw(gl, offset, uniform_offset, uniform_color, attribute_position) {
    gl.bindBuffer(gl.ARRAY_BUFFER, this._vbo)
    gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, this._ebo)
    gl.enableVertexAttribArray(attribute_position)
    gl.vertexAttribPointer(attribute_position, 3, gl.FLOAT, false, 4 * 3, 0)
    gl.uniform3f(uniform_offset, offset[0], offset[1], offset[2])
    gl.uniform4f(uniform_color, 1, 1, 1, 1)
    gl.drawElements(gl.LINES, this._element_count, gl.UNSIGNED_SHORT, 0)
    gl.disableVertexAttribArray(attribute_position)
  }
}
