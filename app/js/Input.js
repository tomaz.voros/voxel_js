export default class Input {
    constructor(element) {
        /**
        * Map of codes that are being watched. Value is the state of the key
        * since the last read. 0 means not pressed. 1 means pressed. 2 means
        * released. Released is reset to not pressed after read.
        * @type {Map<string, number>}
        */
        this._keys = new Map()
        /**
         * Handlers for keys that have handlers attached instead.
         * @type {Map<string, function>}
         */
        this._key_handlers = new Map()
        /**
        * Indicates whether or not a mouse button is pressed. First element
        * indicates main button, second element indicates secondary button.
        * 0 means not pressed. 1 means pressed. 2 means released.
        * Released is reset to not pressed after read.
        * @type {Array<number>}
        */
        this._mouse_buttons = [0, 0]
        /**
        * Positions x and y of the pointer.
        * @type {Array<number>}
        */
        this._mouse_position = [0, 0]
        /**
        * Movement dx and dy of the pointer since last read.
        * @type {Array<number>}
        */
        this._mouse_movement = [0, 0]
        /**
        * Wheel steps since last read in y direction.
        * @type {number}
        */
        this._wheel_y = 0
        /** @type {function|null} */
        this._mouse_down_handler = null
        
        document.addEventListener('pointerlockchange', () => this._release_all())
        element.addEventListener('fullscreenchange', () => this._release_all())
        element.addEventListener('blur', () => this._release_all())
        element.addEventListener('focus', () => this._release_all())
        element.addEventListener('keydown', event => {
            const code_entry = this._keys.get(event.code)
            if (code_entry !== undefined) {
                this._keys.set(event.code, 1)
            } else {
                const handler = this._key_handlers.get(event.code)
                if (handler !== undefined)
                    handler(true)
            }
        })
        element.addEventListener('keyup', event => {
            const code_entry = this._keys.get(event.code)
            if (code_entry !== undefined) {
                if (code_entry === 1)
                    this._keys.set(event.code, 2)
            } else {
                const handler = this._key_handlers.get(event.code)
                if (handler !== undefined)
                    handler(false)
            }
        })
        element.addEventListener('wheel', event => {
            this._wheel_y += Math.sign(event.deltaY)
        })
        element.addEventListener('mousemove', event => {
            this._mouse_position[0] = event.x
            this._mouse_position[1] = event.y
            this._mouse_movement[0] += event.movementX
            this._mouse_movement[1] += event.movementY
        })
        element.addEventListener('mousedown', (event) => {
            if (event.button !== 0 && event.button !== 2)
                return
            const button_index = event.button === 0 ? 0 : 1
            this._mouse_buttons[button_index] = 1
            if (this._mouse_down_handler !== null)
                this._mouse_down_handler()
        })
        element.addEventListener('mouseup', (event) => {
            if (event.button !== 0 && event.button !== 2)
                return
            const button_index = event.button === 0 ? 0 : 1
            if (this._mouse_buttons[button_index] === 1)
                this._mouse_buttons[button_index] = 2
        })
    }
    _release_all() {
        for (const key of this._keys.keys())
            this._keys.set(key, 0)
        this._mouse_buttons[0] = 0
        this._mouse_buttons[1] = 0
        this._mouse_position[0] = 0
        this._mouse_position[1] = 0
        this._mouse_movement[0] = 0
        this._mouse_movement[1] = 0
        this._wheel_y = 0
    }
    /**
    * Adds key to the map of watched keys.
    * @param {string} code Code of the key to be watched.
    * @returns {boolean} True if successfully added. False if already watching.
    */
    watch_code(code) {
        if (this._keys.has(code)) {
            return false
        } else {
            this._keys.set(code, 0)
            return true
        }
    }
    /**
    * Removes key from the map of watched keys.
    * @param {string} code Code of the key to be removed.
    * @returns {boolean} True if successfully removed. False if key not watched.
    */
    unwatch_code(code) {
        return this._keys.delete(code)
    }
    /**
    * Adds key to the map of handled keys.
    * @param {string} code Code of the key to be watched.
    * @param {function} handler Handler to be called on key event. Bool is
    *                           passed to handler indicating pressed true
    *                           or released false.
    * @returns {boolean} True if successfully added. False if already watching.
    */
    watch_code_handler(code, handler) {
        if (this._key_handlers.has(code)) {
            return false
        } else {
            this._key_handlers.set(code, handler)
            return true
        }
    }
    /**
    * Removes key from the map of handled keys.
    * @param {string} code Code of the key to be removed.
    * @returns {boolean} True if successfully removed. False if key not handled.
    */
    unwatch_code_handler(code) {
        return this._key_handlers.delete(code)
    }
    /**
     * @param {function} handler Handler to be called on mouse down event.
     */
    set_mouse_down_handler(handler) {
        this._mouse_down_handler = handler
    }
    remove_mouse_down_handler() {
        this._mouse_down_handler = null
    }
    mouse_down_handler_is_set() {
        return this._mouse_down_handler !== null
    }
    /**
    * @param {string} key_code Code of the checked key.
    * @returns {boolean} True if the key with given code is currently pressed.
    */
    get_key_pressed(key_code) {
        const entry = this._keys.get(key_code)
        if (entry === undefined) return false
        const pressed = entry === 1
        if (entry === 2)
            this._keys.set(key_code, 0)
        return pressed
    }
    /**
    * @param {string} key_code Code of the checked key.
    * @returns {boolean} True if pressed or was pressed.
    */
    get_key_pressed_sticky(key_code) {
        const entry = this._keys.get(key_code)
        if (entry === undefined) return false
        const pressed_sticky = entry === 1 || entry === 2
        if (entry === 2)
            this._keys.set(key_code, 0)
        return pressed_sticky
    }
    /**
    * @returns {Array<number>} Mouse movement in x and y direction.
    */
    get_mouse_movement() {
        const movement = [this._mouse_movement[0], this._mouse_movement[1]]
        this._mouse_movement[0] = 0
        this._mouse_movement[1] = 0
        return movement
    }
    /**
    * @returns {Array<number>} Mouse position x and y.
    */
    get_mouse_position() {
        return [this._mouse_position[0], this._mouse_position[1]]
    }
    /**
    * @param {number} button_index 0 for primary button, 1 for secondary button.
    * @returns {boolean} True if pressed.
    */
    get_mouse_button(button_index) {
        if (button_index !== 0 && button_index !== 1)
            return false
        const state = this._mouse_buttons[button_index]
        if (state === 2)
            this._mouse_buttons[button_index] = 0
        return state === 1
    }
    /**
    * @param {number} button_index 0 for primary button, 1 for secondary button.
    * @returns {boolean} True if pressed or was pressed.
    */
    get_mouse_button_sticky(button_index) {
        if (button_index !== 0 && button_index !== 1)
            return false
        const state = this._mouse_buttons[button_index]
        if (state === 2)
            this._mouse_buttons[button_index] = 0
        return state === 1 || state === 2
    }
    /**
    * @returns {number} Number of steps that the wheel rotated since last read.
    */
    get_wheel_dy() {
        const wheel_y = this._wheel_y
        this._wheel_y = 0
        return wheel_y
    }
}
