'use strict'
// ISSUE(0)
/* export */ class Utility {
    /**
     * @param {WebGL2RenderingContext} gl WebGL context.
     */
    static flush_gl_errors(gl) {
        while (true) {
            const err = gl.getError()
            if (err === gl.NO_ERROR)
                return
            let error_string = 'unknown error'
            switch (err) {
                case gl.INVALID_ENUM:
                    error_string = 'INVALID_ENUM' 
                    break
                case gl.INVALID_VALUE:
                    error_string = 'INVALID_VALUE' 
                    break
                case gl.INVALID_OPERATION:
                    error_string = 'INVALID_OPERATION' 
                    break
                case gl.INVALID_FRAMEBUFFER_OPERATION:
                    error_string = 'INVALID_FRAMEBUFFER_OPERATION' 
                    break
                case gl.OUT_OF_MEMORY:
                    error_string = 'OUT_OF_MEMORY' 
                    break
                case gl.CONTEXT_LOST_WEBGL:
                    error_string = 'CONTEXT_LOST_WEBGL' 
                    break
            }
            console.warn(`WebGL error: ${error_string} (${err})`)
        }
    }

    /**
     * @param {String} src_url
     * @returns {Promise<ImageData>}
     */
    static load_image(src_url) {
        return new Promise((resolve, reject) => {
            const image = new Image()
            image.addEventListener(
                'load',
                event => {
                    const canvas = document.createElement('canvas')
                    const context = canvas.getContext('2d')
                    canvas.width = image.width
                    canvas.height = image.height
                    context.globalCompositeOperation = 'copy'
                    context.drawImage(image, 0, 0)
                    resolve(context.getImageData(0, 0, image.width, image.height))
                }
            )
            image.addEventListener(
                'error',
                event => reject(new Error(`Failed to load ${src_url}`))
            )
            image.src = src_url
        })
    }

    /**
     * @param {String} src_url
     * @returns {Promise<*>}
     */
    static load_json(src_url) {
        return fetch(src_url).then(
            response => {
                if (!response.ok)
                    throw new Error(`Failed to load ${src_url}`)
                return response.json()
            }
        )
    }

    /**
     * @param {String} src_url
     * @returns {Promise<string>}
     */
    static load_text(src_url) {
        return fetch(src_url).then(
            response => {
                if (!response.ok)
                    throw new Error(`Failed to load ${src_url}`)
                return response.text()
            }
        )
    }

    static copy_blocks_3d(src, dst, src_size, dst_size, src_off, dst_off, size) {
        // TODO: optimize by precomputing as much as possible for indices (directly iterate by the right amount, so no multiplication needed and less addition)
        for (let z = 0; z < size[2]; ++z)
            for (let y = 0; y < size[1]; ++y)
                for (let x = 0; x < size[0]; ++x) {
                    const src_index = src_size[1] * src_size[0] * (src_off[2] + z) + src_size[0] * (src_off[1] + y) + (src_off[0] + x)
                    const dst_index = dst_size[1] * dst_size[0] * (dst_off[2] + z) + dst_size[0] * (dst_off[1] + y) + (dst_off[0] + x)
                    dst[dst_index] = src[src_index]
                }
    }

    /**
     * @param {number} n Any whole number between 0 and 2^31-1.
     * @returns True if n is power of 2.
     */
    static is_power_of_2(n) {
        if (n < 1) return false
        return (1 << 31 - Math.clz32(n)) === n
    }

    /**
     * Converts an integer coordinate to a number suitable for Map/Set key.
     * @param {number[]} position Coordinate position.
     * @returns {number} Number that can be used as a key.
     */
    static position_to_key(position) {
        /* exposition
        const x_bits = 19
        const y_bits = 15
        const z_bits = 19
        console.assert(x_bits + y_bits + z_bits <= 53)
        const p1 = (position[0] & 2**x_bits - 1) * 1
        const p2 = (position[1] & 2**y_bits - 1) * 2**x_bits
        const p3 = (position[2] & 2**z_bits - 1) * 2**x_bits * 2**y_bits
        return p1 + p2 + p3
        */
        return (position[0] & 524287) +
               (position[1] &  32767) * 524288 +
               (position[2] & 524287) * 17179869184
    }

    /**
     * Converts a number suitable for Map/Set key to integer coordinate
     * and stores it in position.
     * @param {number} key Number that can be used as a key.
     * @param {number[]} position Output osition.
     */
    static key_to_position(key, position) {
        /* exposition
        const x_bits = 19
        const y_bits = 15
        const z_bits = 19
        console.assert(x_bits + y_bits + z_bits <= 53)
        return [
            key / 1                     << (32-x_bits) >> (32-x_bits),
            key / 2**x_bits             << (32-y_bits) >> (32-y_bits),
            key / 2**x_bits / 2**y_bits << (32-z_bits) >> (32-z_bits),
        ]
        */
        position[0] = key << 13 >> 13
        position[1] = key * 0.0000019073486328125 << 17 >> 17
        position[2] = key * 0.0000000000582076609134674072265625 << 13 >> 13
    }

    static test_key_position() {
        const x_bits = 19
        const y_bits = 15
        const z_bits = 19
        console.assert(x_bits + y_bits + z_bits <= 53)

        const full_loop_check = p => {
            const p_out = [0,0,0]
            Utility.key_to_position(Utility.position_to_key(p), p_out)
            for (let i = 0; i < 3; ++i)
                if (p[i] !== p_out[i])
                    return false
            return true
        }
        
        const tests = [
            [0,0,0],
            [-1,-1,-1],
            [-1,-1, 1],
            [-1, 1,-1],
            [-1, 1, 1],
            [ 1,-1,-1],
            [ 1,-1, 1],
            [ 1, 1,-1],
            [ 1, 1, 1],
            [-(2**(x_bits-1)), -(2**(y_bits-1)), -(2**(z_bits-1))],
            [-(2**(x_bits-1)), -(2**(y_bits-1)),  2**(z_bits-1)-1],
            [-(2**(x_bits-1)),  2**(y_bits-1)-1, -(2**(z_bits-1))],
            [-(2**(x_bits-1)),  2**(y_bits-1)-1,  2**(z_bits-1)-1],
            [ 2**(x_bits-1)-1, -(2**(y_bits-1)), -(2**(z_bits-1))],
            [ 2**(x_bits-1)-1, -(2**(y_bits-1)),  2**(z_bits-1)-1],
            [ 2**(x_bits-1)-1,  2**(y_bits-1)-1, -(2**(z_bits-1))],
            [ 2**(x_bits-1)-1,  2**(y_bits-1)-1,  2**(z_bits-1)-1],
        ]

        for (const test of tests)
            if (!full_loop_check(test))
                console.log(`${test} failed.`)
    }
}

// ISSUE(0)
/* export */ class Timer {
    constructor() {
        this._start = Timer.now()
    }
    /**
     * @returns {number} Seconds since time origin.
     */
    static now() {
        return performance.now() / 1000
    }
    reset() {
        this._start = Timer.now()
    }
    elapsed() {
        const now = Timer.now()
        return now - this._start
    }
}
