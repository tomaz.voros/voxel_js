'use strict'
// TODO: instead of normalized direction, take end point and iterate until that point
// also check out more efficient initialisation where this method is used
// ISSUE(0)
/* export default */ function * Ray(start, normalized_direction) {
  // A Fast Voxel Traversal Algorithm for Ray Tracing by John Amanatides, Andrew Woo
  const block = [Math.floor(start[0]), Math.floor(start[1]), Math.floor(start[2])]
  const step = [Math.sign(normalized_direction[0]), Math.sign(normalized_direction[1]), Math.sign(normalized_direction[2])]
  // TODO: check if it is a problem when delta is 1 / 0
  const delta = [Math.abs(1 / normalized_direction[0]), Math.abs(1 / normalized_direction[1]), Math.abs(1 / normalized_direction[2])]
  const t_max = [start[0] % 1, start[1] % 1, start[2] % 1]
  if (start[0] < 0) t_max[0] += 1
  if (start[1] < 0) t_max[1] += 1
  if (start[2] < 0) t_max[2] += 1

  if (step[0] === 1) t_max[0] = 1 - t_max[0]
  if (step[1] === 1) t_max[1] = 1 - t_max[1]
  if (step[2] === 1) t_max[2] = 1 - t_max[2]

  t_max[0] /= Math.abs(normalized_direction[0])
  t_max[1] /= Math.abs(normalized_direction[1])
  t_max[2] /= Math.abs(normalized_direction[2])

  if (!isFinite(t_max[0])) t_max[0] = Infinity
  if (!isFinite(t_max[1])) t_max[1] = Infinity
  if (!isFinite(t_max[2])) t_max[2] = Infinity

  yield { block_position: [block[0], block[1], block[2]], t: 0 }

  while (true) {
    let i = 0
    let t_next = 0.0
    if (t_max[0] < t_max[1]) {
      if (t_max[0] < t_max[2])
        i = 0
      else
        i = 2
    } else {
      if (t_max[1] < t_max[2])
        i = 1
      else
        i = 2
    }

    const t = t_max[i]
    block[i] += step[i]
    t_max[i] += delta[i]

    yield { block_position: [block[0], block[1], block[2]], t }
  }
}
