'use strict'
// ISSUE(0)
/* export default */ class Algebra {
  static m4() {
    return [
      1, 0, 0, 0,
      0, 1, 0, 0,
      0, 0, 1, 0,
      0, 0, 0, 1
    ]
  }

  static copy_v3(r, a) {
    r[0] = a[0]
    r[1] = a[1]
    r[2] = a[2]
    return r
  }

  static ortho_m4(m, width_y, aspect_ratio, near_z, far_z) {
    const wh = width_y / 2
    const fn = far_z - near_z

    m[0] = 1 / wh
    m[1] = 0
    m[2] = 0
    m[3] = 0

    m[4] = 0
    m[5] = aspect_ratio / wh
    m[6] = 0
    m[7] = 0

    m[8] = 0
    m[9] = 0
    m[10] = -2 / fn
    m[11] = 0

    m[12] = 0
    m[13] = 0
    m[14] = -(far_z + near_z) / fn
    m[15] = 1

    return m
  }

  static v3() {
    return [0, 0, 0]
  }

  static clamp_s(value, min, max) {
    return Math.max(Math.min(value, max), min)
  }

  static to_index_v3(position, size) {
    return position[2] * size[1] * size[0] + position[1] * size[0] + position[0]
  }

  static floor_mod(x, y) {
    return ((x % y) + y) % y
  }

  static floor_mod_v3(r, a, b) {
    r[0] = Algebra.floor_mod(a[0], b[0])
    r[1] = Algebra.floor_mod(a[1], b[1])
    r[2] = Algebra.floor_mod(a[2], b[2])
    return r
  }

  /**
   * Converts from block world position to block index in chunk.
   */
  static position_to_index_v3(position, size) {
    const p = [0, 0, 0]
    Algebra.floor_mod_v3(p, position, size)
    return Algebra.to_index_v3(p, size)
  }

  static add_v3(r, a, b) {
    r[0] = a[0] + b[0]
    r[1] = a[1] + b[1]
    r[2] = a[2] + b[2]
    return r
  }

  static add_v3_s(r, a, s) {
    r[0] = a[0] + s
    r[1] = a[1] + s
    r[2] = a[2] + s
    return r
  }

  static perspective_m4(r, fov_y, aspect_ratio, near_z, far_z) {
    let f = 1 / Math.tan(fov_y / 2)
    let near_far = near_z - far_z

    r[0] = f / aspect_ratio
    r[1] = 0
    r[2] = 0
    r[3] = 0

    r[4] = 0
    r[5] = f
    r[6] = 0
    r[7] = 0

    r[8] = 0
    r[9] = 0
    r[10] = (far_z + near_z) / near_far
    r[11] = -1

    r[12] = 0
    r[13] = 0
    r[14] = 2 * far_z * near_z / near_far
    r[15] = 0

    return r
  }

  static cross_v3(r, a, b) {
    let a0 = a[0], a1 = a[1], a2 = a[2]
    let b0 = b[0], b1 = b[1], b2 = b[2]
    r[0] = a1 * b2 - a2 * b1
    r[1] = a2 * b0 - a0 * b2
    r[2] = a0 * b1 - a1 * b0
    return r
  }

  static sub_v3(r, a, b) {
    r[0] = a[0] - b[0]
    r[1] = a[1] - b[1]
    r[2] = a[2] - b[2]
    return r
  }

  static sub_v3_s(r, a, s) {
    r[0] = a[0] - s
    r[1] = a[1] - s
    r[2] = a[2] - s
    return r
  }

  static dot_v3(a, b) {
    return a[0] * b[0] + a[1] * b[1] + a[2] * b[2]
  }

  static dot_v4(a, b) {
    return a[0] * b[0] + a[1] * b[1] + a[2] * b[2] + a[3] * b[3]
  }

  static square_length_v3(a) {
    return Algebra.dot_v3(a, a)
  }

  static square_length_v4(a) {
    return Algebra.dot_v4(a, a)
  }

  static length_v3(a) {
    return Math.sqrt(Algebra.square_length_v3(a))
  }

  static length_v4(a) {
    return Math.sqrt(Algebra.square_length_v4(a))
  }

  static normal_v3(r, a) {
    let l = Algebra.length_v3(a)
    r[0] = a[0] / l
    r[1] = a[1] / l
    r[2] = a[2] / l
    return r
  }

  static normal_v4(r, a) {
    let l = Algebra.length_v4(a)
    r[0] = a[0] / l
    r[1] = a[1] / l
    r[2] = a[2] / l
    r[3] = a[3] / l
    return r
  }

  static rotation_x_m4(r, theta) {
    Algebra.identity_m4(r)
    const cos_theta = Math.cos(theta)
    const sin_theta = Math.sin(theta)
    r[5] = cos_theta
    r[6] = -sin_theta
    r[9] = sin_theta
    r[10] = cos_theta
    return r
  }

  static rotation_y_m4(r, theta) {
    Algebra.identity_m4(r)
    const cos_theta = Math.cos(theta)
    const sin_theta = Math.sin(theta)
    r[0] = cos_theta
    r[2] = sin_theta
    r[8] = -sin_theta
    r[10] = cos_theta
    return r
  }

  static rotation_z_m4(r, theta) {
    Algebra.identity_m4(r)
    const cos_theta = Math.cos(theta)
    const sin_theta = Math.sin(theta)
    r[0] = cos_theta
    r[1] = -sin_theta
    r[4] = sin_theta
    r[5] = cos_theta
    return r
  }

  static view_m4(r, from, at, world_up) {
    let front = [0, 0, 0]
    let side = [0, 0, 0]
    let up = [0, 0, 0]

    Algebra.sub_v3(front, from, at)
    Algebra.normal_v3(front, front)

    Algebra.cross_v3(side, world_up, front)
    Algebra.normal_v3(side, side)

    Algebra.cross_v3(up, front, side)
    Algebra.normal_v3(up, up)

    r[0] = side[0]
    r[1] = up[0]
    r[2] = front[0]
    r[3] = 0

    r[4] = side[1]
    r[5] = up[1]
    r[6] = front[1]
    r[7] = 0

    r[8] = side[2]
    r[9] = up[2]
    r[10] = front[2]
    r[11] = 0

    r[12] = -Algebra.dot_v3(side, from)
    r[13] = -Algebra.dot_v3(up, from)
    r[14] = -Algebra.dot_v3(front, from)
    r[15] = 1

    return r
  }

  static floor_v3(r, a) {
    r[0] = Math.floor(a[0])
    r[1] = Math.floor(a[1])
    r[2] = Math.floor(a[2])
    return r
  }

  static floor_div_v3(r, a, b) {
    r[0] = Math.floor(a[0] / b[0])
    r[1] = Math.floor(a[1] / b[1])
    r[2] = Math.floor(a[2] / b[2])
    return r
  }

  static div_v2(r, a, b) {
    r[0] = a[0] / b[0]
    r[1] = a[1] / b[1]
    return r
  }

  static mul_v3(r, a, b) {
    r[0] = a[0] * b[0]
    r[1] = a[1] * b[1]
    r[2] = a[2] * b[2]
    return r
  }

  static mul_v3_s(r, a, s) {
    r[0] = a[0] * s
    r[1] = a[1] * s
    r[2] = a[2] * s
    return r
  }

  static all_equal_v3(a, b) {
    return a[0] === b[0] && a[1] === b[1] && a[2] === b[2]
  }

  static identity_m4(r) {
    r[0] = 1
    r[1] = 0
    r[2] = 0
    r[3] = 0

    r[4] = 0
    r[5] = 1
    r[6] = 0
    r[7] = 0

    r[8] = 0
    r[9] = 0
    r[10] = 1
    r[11] = 0

    r[12] = 0
    r[13] = 0
    r[14] = 0
    r[15] = 1

    return r
  }

  static translation_m4(r, v) {
    Algebra.identity_m4(r)
    r[12] = v[0]
    r[13] = v[1]
    r[14] = v[2]
    return r
  }

  static mul_m4(r, a, b) {
    let a00 = a[0], a01 = a[1], a02 = a[2], a03 = a[3]
    let a10 = a[4], a11 = a[5], a12 = a[6], a13 = a[7]
    let a20 = a[8], a21 = a[9], a22 = a[10], a23 = a[11]
    let a30 = a[12], a31 = a[13], a32 = a[14], a33 = a[15]

    let b00 = b[0], b01 = b[1], b02 = b[2], b03 = b[3]
    let b10 = b[4], b11 = b[5], b12 = b[6], b13 = b[7]
    let b20 = b[8], b21 = b[9], b22 = b[10], b23 = b[11]
    let b30 = b[12], b31 = b[13], b32 = b[14], b33 = b[15]

    r[0] = a00 * b00 + a10 * b01 + a20 * b02 + a30 * b03
    r[1] = a01 * b00 + a11 * b01 + a21 * b02 + a31 * b03
    r[2] = a02 * b00 + a12 * b01 + a22 * b02 + a32 * b03
    r[3] = a03 * b00 + a13 * b01 + a23 * b02 + a33 * b03

    r[4] = a00 * b10 + a10 * b11 + a20 * b12 + a30 * b13
    r[5] = a01 * b10 + a11 * b11 + a21 * b12 + a31 * b13
    r[6] = a02 * b10 + a12 * b11 + a22 * b12 + a32 * b13
    r[7] = a03 * b10 + a13 * b11 + a23 * b12 + a33 * b13

    r[8] = a00 * b20 + a10 * b21 + a20 * b22 + a30 * b23
    r[9] = a01 * b20 + a11 * b21 + a21 * b22 + a31 * b23
    r[10] = a02 * b20 + a12 * b21 + a22 * b22 + a32 * b23
    r[11] = a03 * b20 + a13 * b21 + a23 * b22 + a33 * b23

    r[12] = a00 * b30 + a10 * b31 + a20 * b32 + a30 * b33
    r[13] = a01 * b30 + a11 * b31 + a21 * b32 + a31 * b33
    r[14] = a02 * b30 + a12 * b31 + a22 * b32 + a32 * b33
    r[15] = a03 * b30 + a13 * b31 + a23 * b32 + a33 * b33

    return r
  }

  /**
   * 
   * @param {Array<number>} r Result output.
   * @param {Array<number>} a A position.
   * @param {Array<number>} b B position.
   * @param {number} t Interpolation factor 0=>A, 1=>B.
   */
  static lerp_v3(r, a, b, t) {
    r[0] = (1 - t) * a[0] + t * b[0];
    r[1] = (1 - t) * a[1] + t * b[1];
    r[2] = (1 - t) * a[2] + t * b[2];
  }

  /**
   * 
   * @param {Array<number>} r Result output.
   * @param {Array<number>} a A position.
   * @param {Array<number>} b B position.
   * @param {number} tick_a Tick value for A.
   * @param {number} tick_b Tick value for B.
   * @param {number} tick_now Tick value of current tick.
   * @param {number} t_since_tick_now Time since tick_now in seconds.
   * @param {number} t_per_tick Time per tick.
   */
  static lerp_snapshots_v3(r, a, b, tick_a, tick_b, tick_now, t_since_tick_now, t_per_tick) {
    // derived on paper
    const t = (tick_now - tick_a + t_since_tick_now / t_per_tick) / (tick_b - tick_a)
    Algebra.lerp_v3(r, a, b, t)
  }

  /**
   * 
   * @param {number[]} planes 6 x vec4 plane equations of frustum. Aka. 24 numbers.
   * @param {number[]} sphere_center vec3 Center point of sphere.
   * @param {number} sphere_radius Radius of the sphere.
   * @returns {boolean} False if sphere is outside of the frustum. True else.
   */
  static sphere_in_frustum(planes, sphere_center, sphere_radius) {
    const negative_sphere_radius = -sphere_radius
    for (let i = 0; i < 24; i += 4) {
      // this is a dot product, not using function dot because JavaScript is clumsy
      const plane_point_distance_squared = 
        planes[i + 0] * sphere_center[0] +
        planes[i + 1] * sphere_center[1] +
        planes[i + 2] * sphere_center[2] +
        planes[i + 3]
      if (plane_point_distance_squared < negative_sphere_radius)
        return false
    }
    return true
  }

  /**
   * Transposes matrix m and stores the result in r.
   * @param {number[]} r Output 4x4 matrix.
   * @param {number[]} m Input 4x4 matrix.
   * @returns r
   */
  static transpose_m4(r, m) {
    let m00 = m[0], m01 = m[1], m02 = m[2], m03 = m[3]
    let m10 = m[4], m11 = m[5], m12 = m[6], m13 = m[7]
    let m20 = m[8], m21 = m[9], m22 = m[10], m23 = m[11]
    let m30 = m[12], m31 = m[13], m32 = m[14], m33 = m[15]
    r[0] = m00; r[1] = m10; r[2] = m20; r[3] = m30
    r[4] = m01; r[5] = m11; r[6] = m21; r[7] = m31
    r[8] = m02; r[9] = m12; r[10] = m22; r[11] = m32
    r[12] = m03; r[13] = m13; r[14] = m23; r[15] = m33
    return r
  }

  /**
   * Normalizes a 3d plane.
   * @param {number[]} r output array vec4.
   * @param {number[]} p Input plane vec4.
   * @returns r
   */
  static normalize_plane_v3_v4(r, p) {
    // note length_v3 not length_v4
    const length = Algebra.length_v3(p)
    r[0] = p[0] / length
    r[1] = p[1] / length
    r[2] = p[2] / length
    r[3] = p[3] / length
    return r
  }

  /**
   * 
   * @param {number[]} r Output array of 16 numbers.
   * @param {number[]} matrix 4x4 MVP matrix.
   * @returns r
   */
  static matrix_to_normalized_frustum_planes(r, matrix) {
    // clumsy JavaScript

    // transposed copy
    const matrix_0 = [matrix[0], matrix[4], matrix[8],  matrix[12]]
    const matrix_1 = [matrix[1], matrix[5], matrix[9],  matrix[13]]
    const matrix_2 = [matrix[2], matrix[6], matrix[10], matrix[14]]
    const matrix_3 = [matrix[3], matrix[7], matrix[11], matrix[15]]

    r[0] = matrix_3[0] + matrix_0[0]
    r[1] = matrix_3[1] + matrix_0[1]
    r[2] = matrix_3[2] + matrix_0[2]
    r[3] = matrix_3[3] + matrix_0[3]
    
    r[4] = matrix_3[0] - matrix_0[0]
    r[5] = matrix_3[1] - matrix_0[1]
    r[6] = matrix_3[2] - matrix_0[2]
    r[7] = matrix_3[3] - matrix_0[3]
    
    r[8]  = matrix_3[0] - matrix_1[0]
    r[9]  = matrix_3[1] - matrix_1[1]
    r[10] = matrix_3[2] - matrix_1[2]
    r[11] = matrix_3[3] - matrix_1[3]
    
    r[12] = matrix_3[0] + matrix_1[0]
    r[13] = matrix_3[1] + matrix_1[1]
    r[14] = matrix_3[2] + matrix_1[2]
    r[15] = matrix_3[3] + matrix_1[3]
    
    r[16] = matrix_3[0] + matrix_2[0]
    r[17] = matrix_3[1] + matrix_2[1]
    r[18] = matrix_3[2] + matrix_2[2]
    r[19] = matrix_3[3] + matrix_2[3]
    
    r[20] = matrix_3[0] - matrix_2[0]
    r[21] = matrix_3[1] - matrix_2[1]
    r[22] = matrix_3[2] - matrix_2[2]
    r[23] = matrix_3[3] - matrix_2[3]

    // clumsy JavaScript !!!

    for (let i = 0; i < 24; i += 4) {
      const r_tmp = [r[i + 0], r[i + 1], r[i + 2], r[i + 3]]
      Algebra.normalize_plane_v3_v4(r_tmp, r_tmp)
      r[i + 0] = r_tmp[0]
      r[i + 1] = r_tmp[1]
      r[i + 2] = r_tmp[2]
      r[i + 3] = r_tmp[3]
    }
    return r
  }

}
