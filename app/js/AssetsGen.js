import Assets from './Assets.js'
// ISSUE(0)
// import Utility from './Utility.js'

export default async function init_assets_gen() {
    const db = await open_database()
    update_table(db)
    const form = document.querySelector('form')
    const button = document.createElement('button')
    button.setAttribute('type', 'button')
    button.appendChild(document.createTextNode('generate'))
    button.addEventListener('click', async event => {
        const assets_url = document.querySelector('#url').value
        const assets_name = document.querySelector('#name').value
        const atlas = await generate_assets(assets_url)
        store_atlas(db, atlas, assets_url, assets_name)
        update_table(db)
    })
    form.appendChild(button)
}

/**
 * @returns {Promise<IDBDatabase>}
 */
async function open_database() {
    return new Promise((resolve, reject) => {
        const idb = window.indexedDB
        const idb_request = idb.open('myDB', 1)
        idb_request.onsuccess = event => resolve(event.target.result)
        idb_request.onerror = event => reject(new Error('Failed to open database.'))
        idb_request.onupgradeneeded = event => {
            /** @type {IDBDatabase} */
            const db = event.target.result
            const object_store = db.createObjectStore('atlasses', { keyPath: 'url' })
        }
        idb_request.onblocked = event => {
            reject(new Error('Blocked opening database.'))
        }
    })
}

async function generate_assets(url) {
    const atlas_json = await Utility.load_json(url)
    const atlas_data = Assets.generate_atlas_data(atlas_json)
    const atlas = await Assets.generate_atlas(
        atlas_data.tiles,
        atlas_data.tiles_per_row,
        atlas_data.tile_width
    )
    return atlas
}

/**
 * @param {IDBDatabase} db
 * @param {ImageData} atlas
 * @param {String} url
 * @param {String} name
 */
function store_atlas(db, atlas, url, name) {
    console.log('storing yayy')
    const transaction = db.transaction('atlasses', 'readwrite')
    const object_store = transaction.objectStore('atlasses')
    object_store.add({ url, atlas, name })

    // TOOD: do I wait here for completion?
    const meaning = 42
}

/**
 * 
 * @param {IDBDatabase} db
 */
function update_table(db) {
    const table = document.querySelector('#table')
    table.innerHTML = ''
    const transaction = db.transaction('atlasses', 'readonly')
    const object_store = transaction.objectStore('atlasses')
    object_store.openCursor().onsuccess = event => {
        const cursor = event.target.result;
        if (cursor) {
            const name_node = document.createElement('td')
            name_node.appendChild(document.createTextNode(cursor.value.name))
            const url_node = document.createElement('td')
            url_node.appendChild(document.createTextNode(cursor.value.url))
            const tr = document.createElement('tr')
            tr.appendChild(name_node)
            tr.appendChild(url_node)
            table.appendChild(tr)

            cursor.continue()
        }
    }
}