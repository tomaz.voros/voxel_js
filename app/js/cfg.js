// ISSUE(0)
'use strict'

const cfg = {
    CHUNK_SIZE: [32, 32, 32],
    CHUNK_VOLUME: 32 * 32 * 32,
    JUMBO_CHUNK_SIZE: [34, 34, 34],
    JUMBO_CHUNK_VOLUME: 34 * 34 * 34,
    CHUNK_LOADING_RADIUS: 6, // load closer or equal to this value
    CHUNK_UNLOADING_RADIUS: 8, // unload further than this value
    BLOCK_UPDATE_HIGH_PRIORITY_DISTANCE: 63,
    MESH_GENERATION_LOOP_DURATION_CAP: 0.05,
    // must be equal to CHUNK_SIZE
    MESH_SIZE: [32, 32, 32],
    // must be between (excluding) [0, 0, 0] and (excluding) CHUNK_SIZE
    MESH_OFFSET: [16, 16, 16],
    CHUNK_REQUEST_QUEUE_LENGTH_LIMIT: 64,
    RAY_QUEUE_LENGTH_LIMIT: 3,
    
    MAX_VAO_VBO_CACHE_SIZE: 128,
    
    RAY_LENGTH: 10,
    TEXTUPDATES_PER_SECOND: 1,
    
    COMPRESSION_STRATEGY: {
        IDENTITY: 0,
        DEFLATE: 1,
    },
    
    MESSAGE_ID: {
        CHUNK_DATA: 0,
        ENTITY_DATA: 1,
        CHUNK_REQUEST_DATA: 2,
        BLOCK_DATA: 3,
        REASSIGN_ENTITY_ID: 4,
        DEAD_ENTITIES: 5,
        STOP_SERVER: 6,
        CLIENT_DISCONNECTED: 7,
        NEW_TICK: 8,
        AUTHENTICATE: 9,
        PLAYER_ORIENTATION: 10,
        MOVE_AWARE_AREA: 11,
        AWARE_AREA_OUT_OF_SYNC: 12,
        ADD_ENTITIES: 13,
        REMOVE_ENTITIES: 14,
        // do not forget to update
        ID_COUNT: 15
    },
    
    ENTITY_TYPE: {
        BASE: 0,
        PLAYER: 1,
        BLOB: 2,
        // do not forget to update
        TYPE_COUNT: 3
    },
    
    TICK_DT: 0.05 //seconds
}

// ISSUE(0)
// export default cfg;
