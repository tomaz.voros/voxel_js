'use strict'

// ISSUE(0)
importScripts('./Algebra.js')
importScripts('./cfg.js')
importScripts('./Ray.js')
importScripts('./Utility.js')

importScripts('../external/pako/pako.min.js')

class Chunk {
    constructor(position) {
        this.position = Algebra.v3()
        Algebra.copy_v3(this.position, position)
        this.tick = 0
        // TODO: support compressed blocks (or even 'missing' for far chunks)
        this.blocks = new Uint8Array(cfg.CHUNK_VOLUME)
    }
}

class ChunkMesh {
    constructor(position) {
        this.position = Algebra.v3()
        Algebra.copy_v3(this.position, position)
        this.up_to_date = false
        this.renderer_has = false
        // bitmap of all loaded chunks
        this.chunks = 0
    }
}

class ChunkMesher {
    constructor(tex_coord, transp) {
        this.tex_lut = new Uint8Array(tex_coord)
        // used for meshing, needed 1x per worker
        this.jumbo_chunk = new Uint8Array(cfg.JUMBO_CHUNK_VOLUME)
        this.transp = transp
        this.tex_lut_size = this.tex_lut.length / 48
        if (this.tex_lut.length % 48 != 0)
            console.log('texture coord lut bad')
        if (this.transp.length != this.tex_lut_size)
            console.log('Warning mismatching arrays of blocks and block transparencies.')
        this.vert_off = new Int8Array([
            0,0,0,
            0,0,1,
            0,1,1,
            0,1,0,

            1,0,1,
            1,0,0,
            1,1,0,
            1,1,1,

            1,0,1,
            0,0,1,
            0,0,0,
            1,0,0,

            0,1,1,
            1,1,1,
            1,1,0,
            0,1,0,

            1,0,0,
            0,0,0,
            0,1,0,
            1,1,0,

            0,0,1,
            1,0,1,
            1,1,1,
            0,1,1,
        ])
        this.ao_off = new Int8Array([
             9,  3,  0,
             9, 21, 18,
            15,  3,  6,
            15, 21, 24,

            11, 23, 20,
            11,  5,  2,
            17, 23, 26,
            17,  5,  8,

            19, 11, 20,
            19,  9, 18,
             1, 11,  2,
             1,  9,  0,

            25, 15, 24,
            25, 17, 26,
             7, 15,  6,
             7, 17,  8,

             1,  5,  2,
             1,  3,  0,
             7,  5,  8,
             7,  3,  6,

            19, 21, 18,
            19, 23, 20,
            25, 21, 24,
            25, 23, 26,
        ])
        // neighbor
        this.nb_off = new Int8Array([
            // -x +x -y +y -z +z
            12, 14, 10, 16, 4, 22
        ])
    }

    mesh_chunk(chunks_blocks) {
        // TODO: test and benchmark
        if (
            cfg.MESH_OFFSET[0] * 2 !== cfg.CHUNK_SIZE[0] ||
            cfg.MESH_OFFSET[1] * 2 !== cfg.CHUNK_SIZE[1] ||
            cfg.MESH_OFFSET[2] * 2 !== cfg.CHUNK_SIZE[2] ||
            cfg.MESH_OFFSET[0] !== cfg.MESH_OFFSET[1] ||
            cfg.MESH_OFFSET[0] !== cfg.MESH_OFFSET[2]
        )
        console.log('Unsupported mesh offset, implement or disallow.')

        const mo = cfg.MESH_OFFSET[0]
        const jumbo_chunk = this.jumbo_chunk
        const copy_size = [cfg.MESH_OFFSET[0] + 1, cfg.MESH_OFFSET[1] + 1, cfg.MESH_OFFSET[2] + 1]
        Utility.copy_blocks_3d(chunks_blocks[0], jumbo_chunk, cfg.CHUNK_SIZE, cfg.JUMBO_CHUNK_SIZE, [mo-1, mo-1, mo-1], [   0,    0,    0], copy_size)
        Utility.copy_blocks_3d(chunks_blocks[1], jumbo_chunk, cfg.CHUNK_SIZE, cfg.JUMBO_CHUNK_SIZE, [   0, mo-1, mo-1], [mo+1,    0,    0], copy_size)
        Utility.copy_blocks_3d(chunks_blocks[2], jumbo_chunk, cfg.CHUNK_SIZE, cfg.JUMBO_CHUNK_SIZE, [mo-1,    0, mo-1], [   0, mo+1,    0], copy_size)
        Utility.copy_blocks_3d(chunks_blocks[3], jumbo_chunk, cfg.CHUNK_SIZE, cfg.JUMBO_CHUNK_SIZE, [   0,    0, mo-1], [mo+1, mo+1,    0], copy_size)
        Utility.copy_blocks_3d(chunks_blocks[4], jumbo_chunk, cfg.CHUNK_SIZE, cfg.JUMBO_CHUNK_SIZE, [mo-1, mo-1,    0], [   0,    0, mo+1], copy_size)
        Utility.copy_blocks_3d(chunks_blocks[5], jumbo_chunk, cfg.CHUNK_SIZE, cfg.JUMBO_CHUNK_SIZE, [   0, mo-1,    0], [mo+1,    0, mo+1], copy_size)
        Utility.copy_blocks_3d(chunks_blocks[6], jumbo_chunk, cfg.CHUNK_SIZE, cfg.JUMBO_CHUNK_SIZE, [mo-1,    0,    0], [   0, mo+1, mo+1], copy_size)
        Utility.copy_blocks_3d(chunks_blocks[7], jumbo_chunk, cfg.CHUNK_SIZE, cfg.JUMBO_CHUNK_SIZE, [   0,    0,    0], [mo+1, mo+1, mo+1], copy_size)

        const vert_off = this.vert_off
        const ao_off = this.ao_off
        const nb_off = this.nb_off
        const tex_lut = this.tex_lut
        const mesh = []
        let element_count = 0
        const ii = Algebra.v3()
        const from = [1, 1, 1]
        const to = Algebra.v3()
        Algebra.add_v3(to, from, cfg.CHUNK_SIZE)
        const not_air = new Array(27) // boolArray() ?

        const off_correction = Algebra.v3()
        Algebra.copy_v3(off_correction, cfg.MESH_OFFSET)

        const next_z = cfg.JUMBO_CHUNK_SIZE[1] * cfg.JUMBO_CHUNK_SIZE[0]
        const next_y = cfg.JUMBO_CHUNK_SIZE[0]
        const next_x = 1

        const pp = Algebra.v3()
        const from_t = [next_x * from[0], next_y * from[1], next_z * from[2]]
        const to_t = [next_x * to[0], next_y * to[1], next_z * to[2]]

        for (ii[2] = from_t[2], pp[2] = off_correction[2]; ii[2] < to_t[2]; ii[2] += next_z, ++pp[2]) {
            for (ii[1] = from_t[1], pp[1] = off_correction[1]; ii[1] < to_t[1]; ii[1] += next_y, ++pp[1]) {
                for (ii[0] = from_t[0], pp[0] = off_correction[0]; ii[0] < to_t[0]; ii[0] += next_x, ++pp[0]) {
                    const block_index = ii[0] + ii[1] + ii[2]
                    const type = jumbo_chunk[block_index]
                    if (type === 0)
                        continue

                    let j = 0
                    const block_position = Algebra.v3()
                    for (block_position[2] = ii[2] - next_z; block_position[2] <= ii[2] + next_z; block_position[2] += next_z) {
                        for (block_position[1] = ii[1] - next_y; block_position[1] <= ii[1] + next_y; block_position[1] += next_y) {
                            for (block_position[0] = ii[0] - next_x; block_position[0] <= ii[0] + next_x; block_position[0] += next_x) {
                                const block_index = block_position[0] + block_position[1] + block_position[2]
                                const type = jumbo_chunk[block_index]
                                if (type > 0 && type < this.transp.length)
                                    not_air[j++] = !this.transp[type]
                                else
                                    not_air[j++] = type !== 0
                            }
                        }
                    }

                    const ttype = Math.min(this.tex_lut_size - 1, type)
                    const tex_off = ttype * 48

                    for (let side = 0; side < 6; ++side) {
                        if (not_air[nb_off[side]])
                            continue
                        const ao_0 = ChunkMesher.vertex_AO(not_air[ao_off[side * 12 + 0]], not_air[ao_off[side * 12 +  1]], not_air[ao_off[side * 12 +  2]])
                        const ao_1 = ChunkMesher.vertex_AO(not_air[ao_off[side * 12 + 3]], not_air[ao_off[side * 12 +  4]], not_air[ao_off[side * 12 +  5]])
                        const ao_2 = ChunkMesher.vertex_AO(not_air[ao_off[side * 12 + 6]], not_air[ao_off[side * 12 +  7]], not_air[ao_off[side * 12 +  8]])
                        const ao_3 = ChunkMesher.vertex_AO(not_air[ao_off[side * 12 + 9]], not_air[ao_off[side * 12 + 10]], not_air[ao_off[side * 12 + 11]])
                        const ao_packed = ao_0 << 6 | ao_1 << 4 | ao_2 << 2 | ao_3
                        
                        mesh.push(pp[0] + vert_off[side * 12 + 0]); mesh.push(pp[1] + vert_off[side * 12 +  1]); mesh.push(pp[2] + vert_off[side * 12 +  2]); mesh.push(ao_packed); mesh.push(tex_lut[tex_off + 8 * side + 0]); mesh.push(tex_lut[tex_off + 8 * side + 1])
                        mesh.push(pp[0] + vert_off[side * 12 + 3]); mesh.push(pp[1] + vert_off[side * 12 +  4]); mesh.push(pp[2] + vert_off[side * 12 +  5]); mesh.push(ao_packed); mesh.push(tex_lut[tex_off + 8 * side + 2]); mesh.push(tex_lut[tex_off + 8 * side + 3])
                        mesh.push(pp[0] + vert_off[side * 12 + 6]); mesh.push(pp[1] + vert_off[side * 12 +  7]); mesh.push(pp[2] + vert_off[side * 12 +  8]); mesh.push(ao_packed); mesh.push(tex_lut[tex_off + 8 * side + 4]); mesh.push(tex_lut[tex_off + 8 * side + 5])
                        mesh.push(pp[0] + vert_off[side * 12 + 9]); mesh.push(pp[1] + vert_off[side * 12 + 10]); mesh.push(pp[2] + vert_off[side * 12 + 11]); mesh.push(ao_packed); mesh.push(tex_lut[tex_off + 8 * side + 6]); mesh.push(tex_lut[tex_off + 8 * side + 7])
                        element_count += 6
                    }
                }
            }
        }

        // TODO: benchmark vs built-in "new Int8Array(mesh)"
        const mesh_buffer = new ArrayBuffer(element_count * 6)
        const mesh_buffer_view = new DataView(mesh_buffer)
        for (let i = 0; i < element_count; ++i) {
            // 3 position, 1 ao, 2 texture
            mesh_buffer_view.setUint8(i * 6 + 0, mesh[i * 6 + 0])
            mesh_buffer_view.setUint8(i * 6 + 1, mesh[i * 6 + 1])
            mesh_buffer_view.setUint8(i * 6 + 2, mesh[i * 6 + 2])
            mesh_buffer_view.setUint8(i * 6 + 3, mesh[i * 6 + 3])
            mesh_buffer_view.setUint8(i * 6 + 4, mesh[i * 6 + 4])
            mesh_buffer_view.setUint8(i * 6 + 5, mesh[i * 6 + 5])
        }

        return { mesh_buffer, element_count }
    }

    static vertex_AO(side_a, side_b, corner) {
        // also correct: return side_a + side_b + (side_a && side_b || corner)
        let strength = 0
        if (side_a && side_b)
            strength = 3
        else
            strength = side_a + side_b + corner
        return strength
    }
}

class ChunkIterator {
    // TODO: allow seeking to a previous position (so tht restart() is not the only option)

    /**
     * @param {number} radius Radius of the iterated sphere.
     */
    constructor(radius) {
        const point = [0,0,0]
        const points = []
        const radius_squared = radius * radius
        for (point[2] = -radius; point[2] <= radius; ++point[2])
        for (point[1] = -radius; point[1] <= radius; ++point[1])
        for (point[0] = -radius; point[0] <= radius; ++point[0]) {
            if (Algebra.square_length_v3(point) > radius_squared)
                continue
            points.push(point.slice())
        }
        points.sort((p1, p2) => ChunkIterator.compare_points_before(p1, p2) ? -1 : 1)
        this._points = new Int8Array(points.length * 3)
        this._index = 0
        this._size = points.length
        let i = 0
        for (const point of points) {
            this._points[i++] = point[0]
            this._points[i++] = point[1]
            this._points[i++] = point[2]
        }
    }

    /**
     * @param {number[]} r Stores the next chunk offset.
     * @returns {boolean} True if valid or false if end reached.
     */
    next(r) {
        if (this._index < this._points.length) {
            r[0] = this._points[this._index++]
            r[1] = this._points[this._index++]
            r[2] = this._points[this._index++]
            return true
        } else {
            r[0] = 0
            r[1] = 0
            r[2] = 0
            return false
        }
    }

    end_reached() {
        return this._index === this._points.length
    }

    /**
     * Resets the iterator to first chunk offset.
     */
    restart() {
        this._index = 0
    }

    size() {
        return this._size
    }

    /**
     * Computes which point comes first in iterator.
     * It is ordered by:
     *     1) euclidean distance min to max
     *     2) height top to down
     *     3) horizontal angle
     * @param {number[]} p1 Coordinate of point 1.
     * @param {number[]} p2 Coordinate of point 2.
     * @returns {boolean} True if p1 comes before p2, false if not or if equal.
     */
    static compare_points_before(p1, p2) {
        // euclidean distance min to max
        const square_length_1 = Algebra.square_length_v3(p1)
        const square_length_2 = Algebra.square_length_v3(p2)
        if (square_length_1 < square_length_2)
            return true
        if (square_length_1 > square_length_2)
            return false
        // height top to down
        const height_1 = p1[1]
        const height_2 = p2[1]
        if (height_1 > height_2)
            return true
        if (height_1 < height_2)
            return false
        // horizontal angle
        const x1 = p1[0]
        const y1 = p1[2]
        const x2 = p2[0]
        const y2 = p2[2]
        return ChunkIterator.compare_angles_smaller(x1, y1, x2, y2)
    }

    /**
     * @param {number} x1 x1.
     * @param {number} y1 y1.
     * @param {number} x2 x2.
     * @param {number} y2 y2.
     * @returns {boolean} Returns true if angle [x1, y1] is smaller than
     *                    angle [x2, y2], false if equal or bigger.
     */
    static compare_angles_smaller(x1, y1, x2, y2) {
        const sign_x1 = +(x1 < 0)
        const sign_y1 = +(y1 < 0)
        const sign_x2 = +(x2 < 0)
        const sign_y2 = +(y2 < 0)
        const quadrant_1 = sign_y1 << 1 & +(sign_x1 !== sign_y1)
        const quadrant_2 = sign_y2 << 1 & +(sign_x2 !== sign_y2)
        // check quadrants
        if (quadrant_1 < quadrant_2)
            return true
        if (quadrant_1 > quadrant_2)
            return false
        // check angle in quadrant
        return y1 * x2 < x1 * y2
    }
}

class ChunkAndChunkMeshManager {
    /**
     * @param {number[]} texture_coordinates Texture coordinates.
     * @param {number[]} texture_transparency Texture coordinates.
     * @param {WebSocket} web_socket Web socket.
     */
    constructor(texture_coordinates, texture_transparency, web_socket) {
        this._ws = web_socket
        this._chunk_mesher = new ChunkMesher(texture_coordinates, texture_transparency)
        this._chunk_iterator = new ChunkIterator(cfg.CHUNK_LOADING_RADIUS)
        this._max_chunks_loaded = new ChunkIterator(cfg.CHUNK_UNLOADING_RADIUS).size()
        /** @type {Set<number>} */
        this._chunk_request_set = new Set()
        /** @type {Map<number, Chunk>} */
        this._chunk_map = new Map()
        this._center_chunk = Algebra.v3()
        this._center_block = Algebra.v3()
        /** @type {Map<number, ChunkMesh>} */
        this._mesh_map = new Map()
        /** @type {Set<number>} Values are keys to this._mesh_map */
        this._mesh_generation_queue = new Set()
        /** @type {Set<number>} Values are keys to this._mesh_map */
        this._mesh_generation_queue_high_priority = new Set()
        this._tick_value = 0
        this._update_scheduled = false
        this._aware_area_changed = true
    }

    /**
     * @param {number} new_tick_value New tick value.
     */
    update_tick_value(new_tick_value) {
        this._tick_value = new_tick_value
    }

    /**
     * @param {number[]} new_center_block New center block.
     */
    update_center_block(new_center_block) {
        Algebra.copy_v3(this._center_block, new_center_block)
        const new_center_chunk = Algebra.v3()
        Algebra.floor_div_v3(new_center_chunk, new_center_block, cfg.CHUNK_SIZE)
        const center_chunk_changed = !Algebra.all_equal_v3(this._center_chunk, new_center_chunk)
        if (!center_chunk_changed)
            return
        Algebra.copy_v3(this._center_chunk, new_center_chunk)
        this._aware_area_changed = true
        this._chunk_iterator.restart()
        this.schedule_update()
    }

    /**
     * @param {number[]} chunk_position Chunk Position.
     * @returns {Chunk|undefined} Chunk if cached, undefined if not cached.
     */
    get_chunk_if_cached(chunk_position) {
        const chunk_key = Utility.position_to_key(chunk_position)
        return this._chunk_map.get(chunk_key)
    }

    /**
     * Stores the current aware chunk area AABB in from and to.
     * @param {number[]} from Begin point of AABB aware area (inclusive).
     * @param {number[]} to End point of AABB aware area (exclusive).
     * @returns {boolean} True if the area has changed since last call to this function.
     */
    get_aware_area(from, to) {
        Algebra.sub_v3_s(from, this._center_chunk, cfg.CHUNK_UNLOADING_RADIUS)
        Algebra.add_v3_s(to, this._center_chunk, cfg.CHUNK_UNLOADING_RADIUS + 1)
        const aware_area_changed = this._aware_area_changed
        this._aware_area_changed = false
        return aware_area_changed
    }

    /**
     * @param {ArrayBuffer} message Block updates message from server.
     */
    update_blocks(message) {
        const data_view = new DataView(message)
        const block_count = Math.floor((data_view.byteLength - 1) / 16)
        if (block_count * 16 + 1 !== data_view.byteLength)
            console.warn('Broken update_blocks message from server.')

        const block_position = Algebra.v3()
        const chunk_position = Algebra.v3()
        const block_position_in_mesh = Algebra.v3()
        const block_position_in_mesh_iterator = Algebra.v3()
        const mesh_position = Algebra.v3()
        const player_to_block_vector = Algebra.v3()

        const squared_high_priority_distance = cfg.BLOCK_UPDATE_HIGH_PRIORITY_DISTANCE * cfg.BLOCK_UPDATE_HIGH_PRIORITY_DISTANCE

        const mesh_generation_queue_size_before = this._mesh_generation_queue.size
        const mesh_generation_queue_high_priority_size_before = this._mesh_generation_queue_high_priority.size
        for (let i = 0; i < block_count; ++i) {
            block_position[0] = data_view.getInt32(1 + i * 4     , true)
            block_position[1] = data_view.getInt32(1 + i * 4 +  4, true)
            block_position[2] = data_view.getInt32(1 + i * 4 +  8, true)
            const block_type = data_view.getUint32(1 + i * 4 + 12, true)

            Algebra.floor_div_v3(chunk_position, block_position, cfg.CHUNK_SIZE)
            const chunk_key = Utility.position_to_key(chunk_position)
            const chunk = this._chunk_map.get(chunk_key)
            if (chunk === undefined)
                continue // chunk not stored
            const block_index = Algebra.position_to_index_v3(block_position, cfg.CHUNK_SIZE)
            chunk.blocks[block_index] = block_type
            chunk.tick = this._tick_value

            Algebra.sub_v3(player_to_block_vector, this._center_block, block_position)
            const squared_distance_to_block = Algebra.square_length_v3(player_to_block_vector)
            const high_priority = squared_distance_to_block <= squared_high_priority_distance

            Algebra.sub_v3(block_position_in_mesh, block_position, cfg.MESH_OFFSET)

            for (block_position_in_mesh_iterator[2] = block_position_in_mesh[2] - 1; block_position_in_mesh_iterator[2] <= block_position_in_mesh[2] + 1; ++block_position_in_mesh_iterator[2])
                for (block_position_in_mesh_iterator[1] = block_position_in_mesh[1] - 1; block_position_in_mesh_iterator[1] <= block_position_in_mesh[1] + 1; ++block_position_in_mesh_iterator[1])
                    for (block_position_in_mesh_iterator[0] = block_position_in_mesh[0] - 1; block_position_in_mesh_iterator[0] <= block_position_in_mesh[0] + 1; ++block_position_in_mesh_iterator[0]) {
                        Algebra.floor_div_v3(mesh_position, block_position_in_mesh_iterator, cfg.MESH_SIZE)
                        const mesh_key = Utility.position_to_key(mesh_position)
                        const mesh = this._mesh_map.get(mesh_key)
                        if (mesh === undefined)
                            continue
                        mesh.up_to_date = false
                        if (mesh.chunks === 255) {
                            if (high_priority) {
                                this._mesh_generation_queue.delete(mesh_key)
                                this._mesh_generation_queue_high_priority.add(mesh_key)
                            } else {
                                this._mesh_generation_queue.add(mesh_key)
                            }
                        }
                    }
        }
        if (mesh_generation_queue_size_before === 0 && this._mesh_generation_queue.size > 0)
            this.schedule_update()
        else if (mesh_generation_queue_high_priority_size_before == 0 && this._mesh_generation_queue_high_priority.size > 0)
            this.schedule_update()
    }

    /**
     * @param {ArrayBuffer} message Chunk update message from server.
     */
    update_chunk(message) {
        // TODO: check if the chunk is not outside of the unloading radius
        const data_view = new DataView(message)
        const byte_view = new Uint8Array(message, 14)

        const compression_strategy = data_view.getUint8(13)

        const chunk_position = [
            data_view.getInt32(1, true),
            data_view.getInt32(5, true),
            data_view.getInt32(9, true)
        ]

        let decoded_chunk = null
        if (compression_strategy === cfg.COMPRESSION_STRATEGY.IDENTITY) {
            decoded_chunk = byte_view;
        } else if (compression_strategy === cfg.COMPRESSION_STRATEGY.DEFLATE) {
            decoded_chunk = pako.inflateRaw(byte_view, { windowBits: -15 })
        if (decoded_chunk.length != cfg.CHUNK_VOLUME)
            throw 'Bad chunk data.'
        } else {
            throw 'Compression strategy not supported.'
        }

        // set chunk to new value
        const chunk_key = Utility.position_to_key(chunk_position)
        if (!this._chunk_map.has(chunk_key))
            this._chunk_map.set(chunk_key, new Chunk(chunk_position))
        const chunk = this._chunk_map.get(chunk_key)
        chunk.tick = this._tick_value
        for (let i = 0; i < cfg.CHUNK_VOLUME; ++i)
            chunk.blocks[i] = decoded_chunk[i]

        const chunk_request_set_size_before = this._chunk_request_set.size
        const chunk_request_deleted = this._chunk_request_set.delete(chunk_key)
        if (chunk_request_set_size_before === cfg.CHUNK_REQUEST_QUEUE_LENGTH_LIMIT && chunk_request_deleted)
            this.schedule_update()

        // notify meshes
        const mesh_position = Algebra.v3()
        let mesh_index_flag = 1
        // TODO: handle high priority mesh_updates like in this.update_blocks(...) (when the entire chunk gets changed)
        const mesh_generation_queue_size_before = this._mesh_generation_queue.size
        for (mesh_position[2] = chunk_position[2] - 1; mesh_position[2] <= chunk_position[2]; ++mesh_position[2])
            for (mesh_position[1] = chunk_position[1] - 1; mesh_position[1] <= chunk_position[1]; ++mesh_position[1])
                for (mesh_position[0] = chunk_position[0] - 1; mesh_position[0] <= chunk_position[0]; ++mesh_position[0]) {
                    const mesh_key = Utility.position_to_key(mesh_position)
                    if (!this._mesh_map.has(mesh_key))
                        this._mesh_map.set(mesh_key, new ChunkMesh(mesh_position))
                    const mesh = this._mesh_map.get(mesh_key)
                    mesh.chunks |= mesh_index_flag
                    mesh_index_flag <<= 1
                    mesh.up_to_date = false
                    if (mesh.chunks === 255)
                        this._mesh_generation_queue.add(mesh_key)
                }
        if (mesh_generation_queue_size_before === 0 && this._mesh_generation_queue.size > 0)
            this.schedule_update()
    }

    _send_chunk_requests() {
        const chunk_offset = Algebra.v3()
        const chunk_position = Algebra.v3()
        const chunk_request_message = new DataView(new ArrayBuffer(13))
        chunk_request_message.setUint8(0, cfg.MESSAGE_ID.CHUNK_REQUEST_DATA)
        let reschedule_update = true
        while (true) {
            if (this._chunk_map.size >= this._max_chunks_loaded) {
                reschedule_update = false
                break
            }
            if (this._chunk_request_set.size >= cfg.CHUNK_REQUEST_QUEUE_LENGTH_LIMIT) {
                reschedule_update = false
                break // request queue full
            }
            if (!this._chunk_iterator.next(chunk_offset)) {
                reschedule_update = false
                break // end reached
            }
            Algebra.add_v3(chunk_position, this._center_chunk, chunk_offset)
            const chunk_key = Utility.position_to_key(chunk_position)
            if (this._chunk_map.has(chunk_key))
                continue // already loaded
            if (this._chunk_request_set.has(chunk_key))
                continue // already requested
            // send request
            chunk_request_message.setInt32(1, chunk_position[0], true)
            chunk_request_message.setInt32(5, chunk_position[1], true)
            chunk_request_message.setInt32(9, chunk_position[2], true)
            this._ws.send(chunk_request_message)
            this._chunk_request_set.add(chunk_key)
        }
        if (reschedule_update) // in the current implementation, this is always false
            this.schedule_update()
    }

    _cleanup_chunks() {
        const squared_unloading_radius = cfg.CHUNK_UNLOADING_RADIUS * cfg.CHUNK_UNLOADING_RADIUS
        const relative_position = Algebra.v3()
        const mesh_position = Algebra.v3()
        let reschedule_update = false
        for (const [chunk_key, chunk] of this._chunk_map) {
            Algebra.sub_v3(relative_position, chunk.position, this._center_chunk)
            const squared_distance = Algebra.square_length_v3(relative_position)
            if (squared_distance <= squared_unloading_radius)
                continue
            // notify meshes
            let mesh_index_flag = 1
            for (mesh_position[2] = chunk.position[2] - 1; mesh_position[2] <= chunk.position[2]; ++mesh_position[2])
                for (mesh_position[1] = chunk.position[1] - 1; mesh_position[1] <= chunk.position[1]; ++mesh_position[1])
                    for (mesh_position[0] = chunk.position[0] - 1; mesh_position[0] <= chunk.position[0]; ++mesh_position[0]) {
                        const mesh_key = Utility.position_to_key(mesh_position)
                        const mesh = this._mesh_map.get(mesh_key)
                        console.assert(mesh !== undefined)
                        mesh.chunks &= ~mesh_index_flag
                        mesh_index_flag <<= 1
                        this._mesh_generation_queue.delete(mesh_key)
                        this._mesh_generation_queue_high_priority.delete(mesh_key)
                        if (mesh.chunks === 0)
                            this._cleanup_mesh(mesh_key, mesh)
                        mesh.up_to_date = false
                    }
            // remove out of range chunk
            this._chunk_map.delete(chunk_key)
            reschedule_update = true
        }
        if (reschedule_update)
            this.schedule_update()
    }

    /**
     * @param {number} mesh_key MeshChunk key to _mesh_map.
     * @param {ChunkMesh} mesh MeshChunk to clean up.
     */
    _cleanup_mesh(mesh_key, mesh) {
        console.assert(mesh.chunks === 0)
        // remove from renderer
        if (mesh.renderer_has) {
            postMessage({
                type: 0,
                meshes: [{
                    buffer: null,
                    position: mesh.position.slice(),
                    element_count: 0
                }]
            })
        }
        this._mesh_map.delete(mesh_key)
    }

    schedule_update() {
        if (!this._update_scheduled) {
            setTimeout(() => {
                this._update_scheduled = false
                this._update()
            })
            this._update_scheduled = true
        }
    }

    _update() {
        // TODO: make functions pauseable / resumable for better time management
        this._cleanup_chunks()
        this._send_chunk_requests()
        // TODO: two different mesh update queues
        //           - high priority for nearby meshes and/or block updates
        //           - low priority for far meshes
        this._update_meshes()
    }

    /**
     * @param {ChunkMesh} mesh ChunkMesh for meshing.
     */
    _update_mesh(mesh) {
        console.assert(mesh.chunks === 255)
        console.assert(!mesh.up_to_date)
        const chunk_position = Algebra.v3()
        /** @type {Uint8Array[]} */
        const chunk_blocks = new Array(8)
        let chunk_index = 0
        for (chunk_position[2] = mesh.position[2]; chunk_position[2] <= mesh.position[2] + 1; ++chunk_position[2])
            for (chunk_position[1] = mesh.position[1]; chunk_position[1] <= mesh.position[1] + 1; ++chunk_position[1])
                for (chunk_position[0] = mesh.position[0]; chunk_position[0] <= mesh.position[0] + 1; ++chunk_position[0]) {
                    const chunk_key = Utility.position_to_key(chunk_position)
                    const chunk = this._chunk_map.get(chunk_key)
                    console.assert(chunk !== undefined)
                    chunk_blocks[chunk_index++] = chunk.blocks
                }

        const mesh_data = this._chunk_mesher.mesh_chunk(chunk_blocks)

        const message = {
            type: 0,
            meshes: []
        }

        // TODO: properly implement replacing instead of remove->add
        if (mesh.renderer_has) {
            // remove old mesh from renderer if loaded
            message.meshes.push({
                buffer: null,
                position: mesh.position.slice(),
                element_count: 0
            })
        }
        if (mesh_data.element_count > 0) {
            // add new mesh to renderer if not empty
            message.meshes.push({
                buffer: mesh_data.mesh_buffer,
                position: mesh.position.slice(),
                element_count: mesh_data.element_count
            })
            mesh.renderer_has = true
        } else {
            mesh.renderer_has = false
        }
        // send to renderer
        if (message.meshes.length > 0) {
            const transferables = []
            for (const mesh of message.meshes)
                if (mesh.buffer !== null)
                    transferables.push(mesh.buffer)
            postMessage(message, transferables)
        }
        mesh.up_to_date = true
    }

    _update_meshes() {
        const start_time = Timer.now()

        const generate = queue => {
            let out_of_time = false
            for (const mesh_key of queue) {
                const mesh = this._mesh_map.get(mesh_key)
                console.assert(mesh !== undefined)
                console.assert(mesh.chunks === 255)
                console.assert(mesh.up_to_date === false)
                this._update_mesh(mesh)
                queue.delete(mesh_key)
                if (Timer.now() - start_time > cfg.MESH_GENERATION_LOOP_DURATION_CAP) {
                    out_of_time = true
                    break
                }
            }
            return out_of_time
        }

        const out_of_time = generate(this._mesh_generation_queue_high_priority)

        if (!out_of_time)
            generate(this._mesh_generation_queue)

        if (this._mesh_generation_queue_high_priority.size > 0 || this._mesh_generation_queue.size > 0)
            this.schedule_update()
    }
}

class Logic {
    constructor(web_socket, tex_coord, transp) {
        globalThis.onmessage = message => this.process_message_from_main_thread(message.data)
        // TODO: keep an eye on this.ws.bufferedAmount
        this.ws = web_socket
        this.ws.onmessage = message => this.process_message_from_server(message.data)
        this.chunk_and_chunk_mesh_manager = new ChunkAndChunkMeshManager(tex_coord, transp, this.ws)
        this.tick_value = 0
    }

    process_message_from_server(message) {
        const message_type = new DataView(message).getUint8(0)
        switch (message_type) {
            case cfg.MESSAGE_ID.CHUNK_DATA:
                this.chunk_and_chunk_mesh_manager.update_chunk(message)
                break
            case cfg.MESSAGE_ID.ENTITY_DATA:
                this.update_entities(message)
                break
            case cfg.MESSAGE_ID.BLOCK_DATA:
                this.chunk_and_chunk_mesh_manager.update_blocks(message)
                break
            case cfg.MESSAGE_ID.NEW_TICK:
                this.update_tick(message)
                break
            case cfg.MESSAGE_ID.PLAYER_ORIENTATION:
                this.set_orientation(message)
                break
            case cfg.MESSAGE_ID.AWARE_AREA_OUT_OF_SYNC:
                this.aware_area_out_of_sync(message)
                break
            case cfg.MESSAGE_ID.ADD_ENTITIES:
                this.add_entities(message)
                break
            case cfg.MESSAGE_ID.REMOVE_ENTITIES:
                this.remove_entities(message)
                break
            default:
                console.warn(`Received unexpected or deprecated message type ${message_type}.`)
                break
        }
    }

    aware_area_out_of_sync(message) {
        console.log("Well, you need to refresh, because resync is not implemented yet.");
    }

    add_entities(message) {
        const msg = {
            type: 5,
            data: message
        }
        postMessage(msg, [msg.data])
    }

    remove_entities(message) {
        const msg = {
            type: 6,
            data: message
        }
        postMessage(msg, [msg.data])
    }

    update_entities(message) {
        const msg = {
            type: 3,
            data: message
        }
        postMessage(msg, [msg.data])
    }

    set_orientation(message) {
        const message_view = new DataView(message)
        postMessage({
            type: 2,
            x:     message_view.getFloat32(1 + 0 * 4, true),
            y:     message_view.getFloat32(1 + 1 * 4, true),
            z:     message_view.getFloat32(1 + 2 * 4, true),
            yaw:   message_view.getFloat32(1 + 3 * 4, true),
            pitch: message_view.getFloat32(1 + 4 * 4, true),
            id:    message_view.getInt32(1 + 5 * 4, true)
        })
    }
  
    process_message_from_main_thread(message) {
        switch (message.type) {
            case 1:
                this.cast_ray(message.from, message.to)
                break
            case 2:
                const last = this.cast_ray(message.from, message.to).last
                this.send_block_set(last, message.block_type)
                break
            case 3:
                const before = this.cast_ray(message.from, message.to).before
                this.send_block_set(before, message.block_type)
                break
            case 4:
                this.send_block_set([message.x, message.y, message.z], message.v)
                break
            case 6:
                this.update_player_orientation(message)
                break
            default:
                console.warn(`Received unexpected or deprecated message type ${message.type}.`)
                break
        }
    }

    update_player_orientation(message) {
        const new_player_position = Algebra.v3()
        const new_center_block = Algebra.v3()
        new_player_position[0] = message.x
        new_player_position[1] = message.y
        new_player_position[2] = message.z
        // set new center block
        Algebra.floor_v3(new_center_block, new_player_position)
        this.chunk_and_chunk_mesh_manager.update_center_block(new_center_block)
        // send orientation update to server
        const msg = new DataView(new ArrayBuffer(5 * 4 + 1))
        msg.setUint8(0, cfg.MESSAGE_ID.PLAYER_ORIENTATION)
        msg.setFloat32(1, message.x, true);
        msg.setFloat32(5, message.y, true);
        msg.setFloat32(9, message.z, true);
        msg.setFloat32(13, message.yaw, true);
        msg.setFloat32(17, message.pitch, true);
        this.ws.send(msg)
    }

    cast_ray(from, to) {
        const ray_direction = Algebra.sub_v3(Algebra.v3(), to, from)
        const length = Algebra.length_v3(ray_direction)
        Algebra.normal_v3(ray_direction, ray_direction)
        const ray = Ray(from, ray_direction)
        const before = Algebra.floor_v3(Algebra.v3(), from)
        const last = Algebra.copy_v3(Algebra.v3(), before)
        /** @type{Chunk|null|undefined} */
        let chunk = null
        const chunk_position = Algebra.v3()
        while (true) {
            const i = ray.next().value
            if (i.t > length)
                break
            Algebra.floor_div_v3(chunk_position, i.block_position, cfg.CHUNK_SIZE)
            if (chunk === null || !Algebra.all_equal_v3(chunk.position, chunk_position)) {
                chunk = this.chunk_and_chunk_mesh_manager.get_chunk_if_cached(chunk_position)
                if (chunk === undefined)
                    break
            }
            const block_index = Algebra.position_to_index_v3(i.block_position, cfg.CHUNK_SIZE)
            const block = chunk.blocks[block_index]
            Algebra.copy_v3(before, last)
            Algebra.copy_v3(last, i.block_position)
            if (block !== 0)
                break
        }
        const result = { type: 1, last, before }
        postMessage(result)
        return result
    }

    send_block_set(block_position, block) {
        const message_view = new DataView(new ArrayBuffer(17))
        message_view.setUint8(0, cfg.MESSAGE_ID.BLOCK_DATA)
        message_view.setInt32(1, block_position[0], true)
        message_view.setInt32(5, block_position[1], true)
        message_view.setInt32(9, block_position[2], true)
        message_view.setUint32(13, block, true)
        this.ws.send(message_view)
    }

    /**
     * @param {ArrayBuffer} message Message from server.
     */
    update_tick(message) {
        const message_view = new DataView(message)
        this.tick_value = message_view.getInt32(1, true)
        // notify ChunkAndChunkMeshManager
        this.chunk_and_chunk_mesh_manager.update_tick_value(this.tick_value)
        // notify Main
        postMessage({
            type: 4,
            tick_value: this.tick_value
        })
        // send aware area
        const aware_from = Algebra.v3()
        const aware_to = Algebra.v3()
        const aware_area_changed = this.chunk_and_chunk_mesh_manager.get_aware_area(aware_from, aware_to)
        if (aware_area_changed) {
            const aware_area_message = new DataView(new ArrayBuffer(1 + 4 * 6))
            aware_area_message.setUint8(0, cfg.MESSAGE_ID.MOVE_AWARE_AREA)
            aware_area_message.setInt32(1, aware_from[0], true)
            aware_area_message.setInt32(5, aware_from[1], true)
            aware_area_message.setInt32(9, aware_from[2], true)
            aware_area_message.setInt32(13, aware_to[0], true)
            aware_area_message.setInt32(17, aware_to[1], true)
            aware_area_message.setInt32(21, aware_to[2], true)
            this.ws.send(aware_area_message)
        }
    }
}

function authenticate(user_name, web_socket) {
    const message_view = new DataView(new ArrayBuffer(17))
    message_view.setUint8(0, cfg.MESSAGE_ID.AUTHENTICATE)
    // user name
    for (let i = 0; i < 16; ++i)
        message_view.setInt8(i + 1, 0)

    for (let i = 0; i < Math.min(16, user_name.length); ++i) {
        const c = user_name.charCodeAt(i)
        message_view.setInt8(i + 1, c)
    }
    web_socket.send(message_view)
}

globalThis.onmessage = message => {
    const message_data = message.data
    if (message_data.type != 5)
        return
    
    const web_socket = new WebSocket(message_data.server_url)
    web_socket.binaryType = 'arraybuffer'

    web_socket.onopen = () => {
        authenticate(message_data.user_name, web_socket)
        globalThis['main'] = new Logic(
            web_socket,
            message_data.tex_coord,
            message_data.tex_transp
        )
        globalThis.postMessage({ type: 7 })
    }
}
